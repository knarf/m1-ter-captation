#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

void inter (glm::vec3 afirst, glm::vec3 asecond,
            glm::vec3 bfirst, glm::vec3 bsecond, glm::vec3& i)
{

    glm::vec3
        da = asecond - afirst,
        db = bsecond - bfirst,
        dc = bfirst - afirst;

    afirst  = afirst  + da*400.f;
    asecond = asecond - da*400.f;
    bfirst  = bfirst + db*400.f;
    bsecond = bsecond - db*400.f;
    da = asecond - afirst;
    db = bsecond - bfirst;
    dc = bfirst - afirst;

    // si non coplanaire
    if(abs(glm::dot(dc, glm::cross(da, db))) >= 0.0001) {
        std::cout << "non coplanaire, fix\n";
        glm::vec3 n = glm::normalize(glm::cross(da, db));
        float d = glm::dot(dc, n);
        afirst  += n*(d/2.f);
        asecond += n*(d/2.f);
        bfirst  -= n*(d/2.f);
        bsecond -= n*(d/2.f);
        da = asecond - afirst,
        db = bsecond - bfirst,
        dc = bfirst - afirst;
#define PV(x) std::cout << #x "\t" << x[0] << "  " << x[1] << "  " <<x[2] << "\n"
#define P(x)  std::cout << #x "\t" << x << "\n"
        PV(n);
        PV(afirst);
        PV(asecond);
        PV(bfirst);
        PV(bsecond);
        P(d);
    }

    if(abs(glm::dot(dc, glm::cross(da, db))) >= 0.01) {
        std::cout << "non coplanaire, err\n";
        return;
    }

    glm::vec3 n = glm::cross(da, db);
    float nl = glm::length(n);
    float s = glm::dot(glm::cross(dc, db), n) / (nl*nl);

    if (0.0f <= s&&s <= 1.0f) {
        i = afirst + da * glm::vec3(s,s,s);
        std::cout << "\nintersection à\n";
        std::cout << i[0] << "\n";
        std::cout << i[1] << "\n";
        std::cout << i[2] << "\n";
        return;
    }
    std::cout << "pad d'intersection??\n";
}

int main ()
{
    glm::vec3
        // position camera dans repère monde
        pcamfront(10.f, 0.f, 0.f),

        // position camera dans repère monde
        pcamtop(0.f, 10.f, 0.f),

        // position objet dans repère monde
        pobjet(0.f, 0.f, 0.f),

        // position objet dans le plan projeté de la caméra dans le
        // repère de la caméra
        pobjetcam(0, 0, -1);


    glm::mat4
        // transformation pour la position de la camera
        tcamfront = glm::lookAt(pcamfront, pobjet, glm::vec3(0.f, 1.f, 0.f)),
        tcamtop   = glm::lookAt(pcamtop, pobjet, glm::vec3(-1.f, 0.f, 0.f)),

        // transformation pour la project avec perspective
        tpersp = glm::perspective(100.f, 1.f, 1.f, 100.f);

    glm::vec4
        rayonfront = glm::inverse(tpersp * tcamfront) * glm::vec4(pobjetcam, 1.f),
        rayontop   = glm::inverse(tpersp * tcamtop)   * glm::vec4(pobjetcam, 1.f);

    glm::vec3
        r3fr = glm::vec3(rayonfront[0], rayonfront[1], rayonfront[2]),
        r3tp = glm::vec3(rayontop[0], rayontop[1], rayontop[2]);


    PV(rayonfront);
    PV(rayontop);

    glm::vec3 i;
    inter(pcamfront, r3fr,
          pcamtop, r3tp,
          i);

}
