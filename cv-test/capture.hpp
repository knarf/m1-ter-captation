#ifndef _CAPTURE_H_
#define _CAPTURE_H_

#include <cv.h>
#include <highgui.h>
#include "moyenne.hpp"
#include <glm/glm.hpp>

class Capture
{
    cv::VideoCapture* input;
    cv::VideoWriter* output;
    Balle b;
    Moyenne moy;
    bool capture;
    bool video;
    cv::Mat frame;
    cv::Size vsize;
    glm::mat4 modelview;
    glm::mat4 projection;

    int hmin, smin, vmin, hmax, smax, vmax;
    int flou[2];
    glm::vec3 pos;

    static void detect(const cv::Mat&, std::vector<cv::Vec3f>&);

public:
    cv::Mat& get_current_frame ();
    void start_capture (const std::string& fn);
    void stop_capture ();
    void set_modelview(glm::mat4 m)
    {
        modelview = m;
    }
    void set_projection(glm::mat4 m)
    {
        projection = m;
    }
    void set_pos(glm::vec3 p)
    {
        pos = p;
    }
    glm::vec3 get_rayon(const Balle& b) const;
    void set_hsv_filter(int hmin, int hmax, int smin, int smax, int vmin, int vmax);
    Capture(const char* s = "0");
    bool handle_next_frame();
    Balle get_current_balle(bool getmoy = false);
    static Balle merge_result(const Capture& cfront, const Balle& front, const Capture& ctop, const Balle& top);
};


#endif
