#include "moyenne.hpp"
#include "balle.hpp"
#include <cv.h>

Moyenne::Moyenne (const uint size) : size(size)
{
    const double min = -8, max = 0, pas = abs(min-max)/(double)size;
    somme_coef = 0;
    data.reserve(size);
    coef.reserve(size);
    for(uint i = 0, j = size-1; i < size; i++, j--) {
        double p = min + (i+1)*pas;
        coef[j] = exp(p);
        // std::cout << j << " "<< p<< " "<< coef[j] << '\n';
        somme_coef += coef[j];
    }
}

void Moyenne::add (const Balle& p)
{
    if(data.size() == size)
        data.pop_back();
    data.insert(data.begin(), p);
}

double Moyenne::calc_somme_coef()
{
    double r = 0;
    for(uint i = 0; i < data.size(); i++)
        r += coef[i];
    return r;
}

Balle Moyenne::calc()
{
    // XXX: bug sur la moyenne quand < size points de données
    Balle b;
    uint size = data.size();
    double scoef = data.size() == size ? somme_coef : calc_somme_coef();
    for(uint i = 0; i < size; i++) {
        b.pos.x += data[i].pos.x * coef[i];
        b.pos.y += data[i].pos.y * coef[i];
        b.pos.z += data[i].pos.z * coef[i];
        b.r += data[i].r * coef[i];
    }
    b.pos.x /= scoef;
    b.pos.y /= scoef;
    b.pos.z /= scoef;
    b.r /= scoef;
    return b;
}
