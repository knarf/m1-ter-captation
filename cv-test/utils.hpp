#ifndef _UTILS_
#define _UTILS_

#include <cstdio>
#include <cstdlib>
#define die(...) fprintf(stderr, __VA_ARGS__), fputc('\n', stderr), exit(1)
typedef unsigned int uint;
#define PV(x) std::cout << #x "\t" << x[0] << "  " << x[1] << "  " <<x[2] << "\n"
#define P(x)  std::cout << #x "\t" << x << "\n"

#endif
