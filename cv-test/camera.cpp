#include <cv.h>
#include <highgui.h>
#include <vector>
#include <iostream>
#include <cstdio>

template<class T>
void print_mat (const cv::Mat& m)
{
    for(int y = 0; y < m.rows; y++) {
        for(int x = 0; x < m.cols; x++) {
            float f = m.at<T>(y, x);
            printf("%+.4e ", f);
        }
        putchar('\n');
    }
    putchar('\n');
}

void print_vec3f (const std::vector<cv::Point3f>& v)
{
    for(uint i = 0; i < v.size(); i++) {
        printf("%+.4e ", v[i].x);
        printf("%+.4e ", v[i].y);
        printf("%+.4e ", v[i].z);
        putchar('\n');
    }
    putchar('\n');
}

void print_vec2f (const std::vector<cv::Point2f>& v)
{
    for(uint i = 0; i < v.size(); i++) {
        printf("%+.4e ", v[i].x);
        printf("%+.4e ", v[i].y);
        putchar('\n');
    }
    putchar('\n');
}

class Camera
{
    cv::VideoCapture* cam;
    cv::Mat_<float> intr, extr;
public:
    Camera (int n = 0)
    {
        intr = cv::Mat_<float>(3, 3, 0.f);

        intr.at<float>(0, 0) = 2.7105506628580332e+02;
        intr.at<float>(0, 1) = 0;
        intr.at<float>(0, 2) = 1.5950000000000000e+02;

        intr.at<float>(1, 0) = 0;
        intr.at<float>(1, 1) = 2.7105506628580332e+02;
        intr.at<float>(1, 2) = 1.1950000000000000e+02;

        intr.at<float>(2, 0) = 0;
        intr.at<float>(2, 1) = 0;
        intr.at<float>(2, 2) = 1;

        cam = new cv::VideoCapture(n);
        if(!cam->isOpened())
            std::cerr << "arg\n";
    }

    void calib_extrinsic ()
    {
        cv::namedWindow("w");
        std::vector<cv::Point3f> obj_points;
        std::vector<cv::Point2f> img_points;
        std::vector<cv::Point2f> corners;
        cv::Size size(4, 3);
        float cell_size = 1;

        for(int i = 0; i < size.height; ++i)
            for(int j = 0; j < size.width; ++j)
                obj_points.push_back(cv::Point3f(float(j*cell_size),
                                                 float(i*cell_size), 0.f));

        cv::Mat img, gray;

        while(1) {
            *cam >> img;
            cv::cvtColor(img, gray, CV_BGR2GRAY);

            img_points.clear();
            corners.clear();
            bool found = cv::findChessboardCorners(gray, size, corners,
                                                   CV_CALIB_CB_ADAPTIVE_THRESH
                                                   | CV_CALIB_CB_FILTER_QUADS);

            if(found) {
                cv::cornerSubPix(gray, corners,
                                 cv::Size(11, 11),
                                 cv::Size(-1, -1),
                                 cv::TermCriteria(CV_TERMCRIT_EPS
                                                  | CV_TERMCRIT_ITER, 30, 0.1));
                cv::drawChessboardCorners(img, size, cv::Mat(corners), found);
            }

            cv::imshow("w", img);
            int key = cv::waitKey(15) & 0xff;

            if(key == ' ' && found) {
                cv::Mat distCoeffs = cv::Mat::zeros(4,1,CV_32FC1);
                cv::Mat r;
                cv::Mat rvecs;
                cv::Mat tvecs;


                cv::solvePnP(cv::Mat(obj_points), cv::Mat(corners), intr, distCoeffs, rvecs, tvecs);

                cv::Rodrigues(rvecs, r);


                extr = cv::Mat_<double>(4, 4, 0.f);

                for(int y = 0; y < 3; y++) {
                    for(int x = 0; x < 3; x++)
                        extr.at<double>(y, x) = r.at<double>(y, x);

                    extr.at<double>(y, 3) = tvecs.at<double>(y, 0);
                }

                extr.at<double>(3, 0) = 0.f;
                extr.at<double>(3, 1) = 0.f;
                extr.at<double>(3, 2) = 0.f;
                extr.at<double>(3, 3) = 1.f;
                print_mat<double>(extr);
            }
        }


    }
};

int main ()
{
    Camera cam;
    cam.calib_extrinsic();
}
