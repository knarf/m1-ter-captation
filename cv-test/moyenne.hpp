#ifndef _MOYENNE_
#define _MOYENNE_

#include <cv.h>
#include <vector>
#include "balle.hpp"

class Moyenne
{
public:
    std::vector<Balle> data;
    std::vector<double> coef;
    double somme_coef;
    uint size;

    Moyenne (const uint size);
    void add (const Balle& b);
    double calc_somme_coef();
    Balle calc();
};

#endif
