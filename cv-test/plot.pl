#!/usr/bin/perl

use strict;
use File::Temp qw/mkstemp/;
use Getopt::Std;

my %o;

getopts('fn:', \%o);

sub plot_file {
    my $fn = shift;
    my ($out, $tmp) = mkstemp('/tmp/plotXXXX');
    open my $f, '<', $fn or die "fichier $fn existe pas";
    my $i = 0;
    while(<$f>) {
        next if !/^(\d+)\s+(\d+)/;
        my ($x, $y) = ($1, $2);
        if($o{f}) {
            $y = ($o{n} || 500) - $y;
        }
        printf $out "%d %d\n", $i, $y;
        $i++;
    }

    system 'gnuplot', '-p', '-e', sprintf(q/plot "%s" u 1:2 title "%s"/, $tmp, $fn);
}

for(@ARGV) {
    plot_file($_);
}
