#ifndef _BALLE_
#define _BALLE_

#include <cv.h>

class Balle
{
public:
    Balle(const cv::Point3f& p = cv::Point3f(0,0,0), double r = 0);
    cv::Point3f pos;
    double r;
};

#endif

