#include "capture.hpp"
#include "utils.hpp"
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <vector>
#include <string>

static bool is_number (const char* s)
{
    std::string str(s);
    for(uint i = 0; i < str.size(); i++)
        if(!('0' <= str[i]&&str[i] <= '9'))
            return false;
    return true;
}

Capture::Capture (const char* s)
    : moy(10)
{
    video = !is_number(s);

    if(is_number(s))
        input = new cv::VideoCapture(atoi(s));
    else
        input = new cv::VideoCapture(s);

    if(!input->isOpened())
        die("erreur ouverture %s", video ? s : "camera");

    (*input) >> frame;
    vsize = frame.size();
    output = 0;

    capture = false;
    flou[0] = 4; flou[1] = 3;
    hmin =   2; hmax =  16;
    smin = 163; smax = 255;
    vmin =  16; vmax = 212;
}

bool Capture::handle_next_frame ()
{
    std::vector<cv::Vec3f> cercles;
    cv::Mat filtre, orange;


    if(!input->grab())
        return false;
    input->retrieve(frame, 0);

    // enregistre dans un fichier
    if(capture) {
        *output << frame;
    }

    cv::cvtColor(frame, filtre, CV_BGR2HSV);
    cv::GaussianBlur(filtre, filtre, cv::Size(2*flou[0]+1, 2*flou[0]+1), flou[1], flou[1]);
    cv::inRange(filtre, cv::Scalar(hmin, smin, vmin), cv::Scalar(hmax, smax, vmax), orange);

    detect(orange, cercles);

    if(cercles.size() > 0) {
        b.pos.x = cercles[0][0];
        b.pos.y = cercles[0][1];
        b.pos.z = 0;
        b.r = cercles[0][2];
        moy.add(b);
    }

    return true;
}

cv::Mat& Capture::get_current_frame ()
{
    return frame;
}

void Capture::start_capture (const std::string& fn)
{
    capture = true;
    output = new cv::VideoWriter(fn.c_str(), 0, 30, vsize, true);
    if(!output->isOpened())
        die("start_capture '%s' failed", fn.c_str());
}

void Capture::stop_capture ()
{
    if(output)
        delete output;
    output = 0;
    capture = false;
}

Balle Capture::get_current_balle (bool getmoy)
{
    return getmoy ? moy.calc() : b;
}

void Capture::detect(const cv::Mat& m, std::vector<cv::Vec3f>& v) {

    uchar old = 0;
    int lenBmax = 0;
    int xmax = -1;
    int ymax = -1;

    for(int i = 0; i < m.rows; i++) {
        const uchar* p = m.ptr(i);
        int x = -1;
        int lenB = 0;
        int lenN = 0;
        for(int j = 0; j < m.cols; j++) {
            uchar c = p[j];

            if(c != old) {
                lenB = lenN = 0;
            }
            if(c) {
                lenB++;
                x = j-lenB;
                if(lenB > lenBmax) {
                    lenBmax = lenB;
                    xmax = x;
                    ymax = i;
                }
            }

            else {
                lenN++;
            }
            old = c;
        }
    }

    if(xmax > 0) {
        int r = lenBmax/2;
        v.push_back(cv::Vec3f(xmax+r, ymax, r));
    }
}


glm::vec3 Capture::get_rayon(const Balle& b) const
{
    glm::vec4 r = glm::inverse(projection * modelview)
        * glm::vec4(
                    b.pos.x * 2.f - vsize.width,
                    (vsize.height-b.pos.y) * 2.f  - vsize.height
                    , 1.f, 1.f);
    return glm::vec3(r[0], r[1], r[2]);
}

static void inter (glm::vec3 afirst, glm::vec3 asecond,
            glm::vec3 bfirst, glm::vec3 bsecond, glm::vec3& i)
{

    glm::vec3
        da = asecond - afirst,
        db = bsecond - bfirst,
        dc = bfirst - afirst;

    afirst  = afirst  + da*400.f;
    asecond = asecond - da*400.f;
    bfirst  = bfirst + db*400.f;
    bsecond = bsecond - db*400.f;
    da = asecond - afirst;
    db = bsecond - bfirst;
    dc = bfirst - afirst;

    // si non coplanaire
    if(abs(glm::dot(dc, glm::cross(da, db))) >= 0.0001) {
        std::cerr << "non coplanaire, fix\n";
        glm::vec3 n = glm::normalize(glm::cross(da, db));
        float d = glm::dot(dc, n);
        afirst  += n*(d/2.f);
        asecond += n*(d/2.f);
        bfirst  -= n*(d/2.f);
        bsecond -= n*(d/2.f);
        da = asecond - afirst,
        db = bsecond - bfirst,
        dc = bfirst - afirst;
        // PV(n);
        // PV(afirst);
        // PV(asecond);
        // PV(bfirst);
        // PV(bsecond);
        // P(d);
    }

    if(abs(glm::dot(dc, glm::cross(da, db))) >= 0.01) {
        std::cerr << "non coplanaire, err\n";
        return;
    }

    glm::vec3 n = glm::cross(da, db);
    float nl = glm::length(n);
    float s = glm::dot(glm::cross(dc, db), n) / (nl*nl);

    if (0.0f <= s&&s <= 1.0f) {
        i = afirst + da * glm::vec3(s,s,s);
        // std::cout << "\nintersection à\n";
        // std::cout << i[0] << "\n";
        // std::cout << i[1] << "\n";
        // std::cout << i[2] << "\n";
        return;
    }
    std::cerr << "pad d'intersection??\n";
}


Balle Capture::merge_result (const Capture& cfront, const Balle& front, const Capture& ctop, const Balle& top)
{
    Balle b;
    glm::vec3 i;
    inter(cfront.pos, cfront.get_rayon(front),
          ctop.pos, ctop.get_rayon(top),
          i);

    b.pos.x = i[0];
    b.pos.y = i[1];
    b.pos.z = i[2];
    b.r = front.r;
    return b;
}

void Capture::set_hsv_filter (int hmin, int hmax, int smin, int smax, int vmin, int vmax)
{
    this->hmin = hmin;
    this->hmax = hmax;

    this->smin = smin;
    this->smax = smax;

    this->vmin = vmin;
    this->vmax = vmax;
}

