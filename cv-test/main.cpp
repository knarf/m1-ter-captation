#include <iostream>
#include <fstream>
#include <cv.h>
#include <highgui.h>
#include <unistd.h> // getopt
#include <cstdio>
#include <cstdlib>
#include "capture.hpp"
#include <vector>
#include "utils.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

struct ConfFile {
    struct ConfCam {
        float pos[3];
        float mv[16];
        float pj[16];
    } cam[2];
};

ConfFile load_conf (char* fn)
{
    ConfFile c;
    std::ifstream in(fn);
    if(!in.is_open())
        die("erreur ouverture conf %s", fn);

    for(int cam = 0; cam < 2; cam++) {
        for(int i = 0; i < 3; i++)
            in >> c.cam[cam].pos[i];

        for(int i = 0; i < 16; i++)
            in >> c.cam[cam].mv[i];

        for(int i = 0; i < 16; i++)
            in >> c.cam[cam].pj[i];
    }
    return c;
}

int count_camera_device ()
{
    CvCapture* cap;
    int n = 0;
    while(1) {
        cap = cvCreateCameraCapture(n);
        if(cap == 0)
            break;

        n++;
        cvReleaseCapture(&cap);
    }

    return n;
}

void test_camera_device ()
{
    int max = count_camera_device();
    std::vector<cv::VideoCapture*> cam;
    std::vector<std::string> win;

    if(max <= 0)
        die("Pas de camera sur ce PC :(");

    cam.reserve(max);
    win.reserve(max);

    for(int i = 0; i < max; i++) {
        char name[256];
        sprintf(name, "cam %d", i);
        win.push_back(name);
        cam.push_back(new cv::VideoCapture(i));
        cv::namedWindow(win[i].c_str());
    }

    while(1) {
        for(int i = 0; i < max; i++) {
            cv::Mat f;
            *cam[i] >> f;
            int cx = f.cols/2, cy = f.rows/2;

             cv::line(f,
                     cv::Point(cx-10, cy),
                     cv::Point(cx+10, cy),
                     cv::Scalar(0,0,255));
            cv::line(f,
                     cv::Point(cx, cy-10),
                     cv::Point(cx, cy+10),
                     cv::Scalar(0,0,255));

            cv::imshow(win[i], f);

        }
        if(cv::waitKey(30) >= 0)
            break;
    }

    for(int i = 0; i < max; i++)
        delete cam[i];
}

int main (int argc, char** argv)
{
    // instance de capture par camera
    Capture *front, *top;

    // device par défaut
    std::string cfront = "0";
    std::string ctop   = "1";

    // nom fichier videos par défaut
    std::string vfront = "front.avi";
    std::string vtop = "top.avi";

    // flag options
    bool capture_mode = false;
    bool filtre_rouge = false;
    bool list_mode = false;

    // windows highgui
    const char* wfront = "front";
    const char* wtop = "top";

    // est en train d'enregistrer ?
    bool capture = false;

    if(argc == 1)
        goto help;

    int opt;
    while((opt = getopt(argc, argv, "cf:t:rlh")) != -1) {
        switch(opt) {
        case 'r':
            filtre_rouge = true;
            break;
        case 'c':
            capture_mode = true;
            break;
        case 'f':
            vfront = optarg;
            break;
        case 't':
            vtop = optarg;
            break;
        case 'l':
            list_mode = true;
            break;
        help:
        case 'h':
        default:
            fprintf(stderr,
                    "Usage: %s [-rhcl] [-f front.avi] [-t top.avi] [ID_FRONT ID_TOP]\n"
                    "\t-r\tactiver filtre rouge (orange par défaut)\n"
                    "\t-c\tmode capture (GUI)\n"
                    "\t-l\tmode liste camera (GUI)\n"
                    "\t-t F\tutilise nom de fichier F pour video dessus (défaut %s)\n"
                    "\t-f F\tidem pour devant (défaut %s)\n"
                    "\tID_FRONT\tid de la camera devant (défaut %s)\n"
                    "\tID_TOP\tid de la camera dessus (défaut %s)\n"
                    "\t-h\taide\n\n"
                    "En mode capture, les noms de fichiers sont utilisés pour enregistrer.\n"
                    "En mode normal, ce sont les videos lues en entrée.\n",
                    argv[0], vfront.c_str(), vtop.c_str(), cfront.c_str(), ctop.c_str());
            exit(1);
        }
    }

    // cam device
    if(argc - optind >= 2) {
        cfront = argv[optind];
        ctop = argv[optind+1];
    }

    if(list_mode) {
        test_camera_device();
        exit(0);
    }

    if(capture_mode) {
        // on passe les ID
        front = new Capture(cfront.c_str());
        top = new Capture(ctop.c_str());
        cv::namedWindow(wfront, CV_WINDOW_AUTOSIZE);
        cv::namedWindow(wtop, CV_WINDOW_AUTOSIZE);
    } else {
        // on passe les noms de fichier pour lecture
        front = new Capture(vfront.c_str());
        top = new Capture(vtop.c_str());
    }

    if(filtre_rouge) {
        front->set_hsv_filter(0,4, 18,255,  16,255);
        top->set_hsv_filter(0,4, 18,255,  16,255);
    } else {
        // orange
        front->set_hsv_filter(2,16, 163,255,  16,212);
        top->set_hsv_filter  (2,16, 163,255,  16,212);
    }


    ConfFile conf = load_conf(getenv("CONF"));
    front->set_modelview(glm::make_mat4(conf.cam[0].mv));
    front->set_projection(glm::make_mat4(conf.cam[0].pj));
    front->set_pos(glm::make_vec3(conf.cam[0].pos));
    top->set_modelview(glm::make_mat4(conf.cam[1].mv));
    top->set_projection(glm::make_mat4(conf.cam[1].pj));
    top->set_pos(glm::make_vec3(conf.cam[1].pos));


    while(front->handle_next_frame() && top->handle_next_frame()) {
        Balle bfront = front->get_current_balle();
        Balle btop = top->get_current_balle();
        Balle b = Capture::merge_result(*front, bfront, *top, btop);

        if((capture_mode && capture) || !capture_mode)
            std::cout << b.pos.x << " " << b.pos.y << " " << b.pos.z << "\n";


        if(capture_mode) {
            cv::Mat mfront = front->get_current_frame();
            cv::Point pfront = cv::Point(bfront.pos.x, -bfront.pos.y);
            cv::circle(mfront, pfront, 3, cv::Scalar(51,204,255), -1, 8, 0);
            cv::circle(mfront, pfront, bfront.r, cv::Scalar(0,0,255), 3, 8, 0);

            cv::Mat mtop = top->get_current_frame();
            cv::Point ptop = cv::Point(btop.pos.x, -btop.pos.y);
            cv::circle(mtop, ptop, 3, cv::Scalar(51,204,255), -1, 8, 0);
            cv::circle(mtop, ptop, btop.r, cv::Scalar(0,0,255), 3, 8, 0);

            // disque rouge dans le coin haut-gauche quand on enregistre
            if(capture) {
                cv::circle(mfront, cv::Point(40,40), 20, cv::Scalar(0,0,255), -1);
                cv::circle(mtop, cv::Point(40,40), 20, cv::Scalar(0,0,255), -1);
            }

            cv::imshow(wfront, mfront);
            cv::imshow(wtop, mtop);

            int k = cv::waitKey(10) & 255;
            if(k == ' ') {
                if(capture) {
                    front->stop_capture();
                    top->stop_capture();
                    capture = false;
                    std::cout << "==STOP==\n";
                } else {
                    front->start_capture(vfront);
                    top->start_capture(vtop);
                    capture = true;
                    std::cout << "==START==\n";
                }
            }
            else if(k == 27) // ESC
                break;
        }
    }

    delete front;
    delete top;
}
