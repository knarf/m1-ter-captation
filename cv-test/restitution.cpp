#include <vector>
#include <iostream>
#include <cstdlib>
#include <fstream>

typedef unsigned int uint;
#define die(...) std::cerr << __VA_ARGS__ << "\n", exit(1)

struct point {
    int x, y;
};

void read_file (std::vector<point>& v, const char* fn)
{
    point p = {0};
    std::ifstream h(fn);
    if(!h.is_open())
        die("aaar");

    while((h >> p.x >> p.y).good())
        v.push_back(p);
}

bool approx (int a, int b, int marge)
{
    return abs(a - b) <= marge;
}

//créé un tableau à partir de v sans les points hors de position
std::vector<point> suprAber(std::vector<point> v, int tolereX, int tolereY)
{
    std::vector<point> res, supr;
    point old = v[0];
    for(uint i = 1; i < v.size(); i++) {
        if( abs(old.x-v[i].x) < tolereX && 
            abs(old.y-v[i].y) < tolereY )
            res.push_back(v[i]);
        else
            supr.push_back(v[i]);
    }

    //feedback
    std::cout<< supr.size() << " valeur(s) suprimée(s)" << std::endl;
    for(uint i = 0; i<supr.size(); i++) {
        std::cout << i <<") " << supr[i].x << " " << supr[i].y << std::endl;
    }
    std::cout << std::endl;

    return res;
}

void compute (std::vector<point>& v)
{
    const int nbhaut = 5;
    int max = v[0].y, min = v[0].y;
    int old;
    int haut[nbhaut];
    int bas[nbhaut];

    for(uint i = 1; i < v.size(); i++) {
        int& c = v[i].y;
        if(c > max)
            max = c;
        if(c < min)
            min = c;
    }

    bool desc = true;
    int nbdesc = 1;

    for(uint i = 1; i < v.size(); i++) {
        old = v[i-1].y;
        int& c = v[i].y;

        // on saute les premieres valeurs
        if(approx(c, min, 30))
            continue;

        // on vient de passer le chibwibwi

        // monte
        if(old > c) {
            if(desc)
                bas[nbdesc-1] = old;
            desc = false;
            continue;
        }

        // on descent pour la 2+eme fois si on arrive la
        if(!desc) {
            haut[nbdesc-1] = old;
            nbdesc++;
            desc = true;
            if(nbdesc > nbhaut)
                break;
        }
    }
    
    std::cout << "Valeur de restitution de la balle (pour chaque rebond)" << std::endl;
    for(int i = 1; i < nbhaut; i++) {
        int old = bas[i-1] - haut[i-1];
        int h = bas[i] - haut[i];
        std::cout << i << ") " << ((double)(h)/old) << '\n';
    }
}

int main(int argc, char** argv)
{
    std::vector<point> v;
    read_file(v, argc >= 2 ? argv[1] : "cap_ok");
    v = suprAber(v, 100, 300);
    compute(v);
}
