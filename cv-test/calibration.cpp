#include <cv.h>
#include <highgui.h>
#include <cstdio>
#include <vector>
#include <cmath>
#include <iostream>

#define die(...) fprintf(stderr, __VA_ARGS__), fputc('\n', stderr), exit(1)
typedef unsigned int uint;

using namespace cv;

struct point {
    int x, y, r;
};

class Moyenne {
    std::vector<point> data;
    std::vector<double> coef;
    double somme_coef;
    uint size;
public:
    Moyenne (const uint size) : size(size)
    {
        const double min = -8, max = 0, pas = abs(min-max)/(double)size;
        somme_coef = 0;
        data.reserve(size);
        coef.reserve(size);
        for(uint i = 0, j = size-1; i < size; i++, j--) {
            double p = min + (i+1)*pas;
            coef[j] = exp(p);
            // std::cout << j << " "<< p<< " "<< coef[j] << '\n';
            somme_coef += coef[j];
        }
    }

    void add (const point& p)
    {
        if(data.size() == size)
            data.pop_back();
        data.insert(data.begin(), p);
    }

    double calc_somme_coef()
    {
        double r = 0;
        for(uint i = 0; i < data.size(); i++)
            r += coef[i];
        return r;
    }

    point calc()
    {
        // XXX: bug sur la moyenne quand < size points de données
        double x = 0, y = 0, r = 0;
        uint size = data.size();
        double scoef = data.size() == size ? somme_coef : calc_somme_coef();
        for(uint i = 0; i < size; i++) {
            x += data[i].x * coef[i];
            y += data[i].y * coef[i];
            r += data[i].r * coef[i];
        }
        x /= scoef;
        y /= scoef;
        r /= scoef;

        return (point){x,y,r};
    }
};


/* Variables */
VideoCapture *cap = 0;
Size tailleVid(0,0);
VideoWriter *outVid = 0;
Moyenne moyenne(5);
int hmin[2] = {2,0}, smin[2] = {163,104}, vmin[2] = {16,29};
int hmax[2] = {16,25}, smax[2] = {255, 255}, vmax[2] = {212,255};

std::vector<point> pointcapture;
bool capture = false;

int p1 = 6, p2 = 12;
int f1 = 9, f2 = 3;
int erosion_size = 3;
int erosion_type = MORPH_ELLIPSE;

/* Fonction */
//algo de detection de cercle
void detect(const Mat& m, std::vector<Vec3f>& v);
//ecriture de la position des cercles capturés
void write_capture();
//action quand on appui sur espace
void cb_button();
//ouverture de la sortie video
void init_outVid();
//appelée lors de la modification de la trackbar d'avancement dans la vidéo
void cb_tbFrame(int, void*);

static bool is_number (const char* s)
{
    std::string str(s);
    for(uint i = 0; i < str.size(); i++)
        if(!('0' <= str[i]&&str[i] <= '9'))
            return false;
    return true;
}

int main(int argc, char **argv) {
    //ouverture de la fenetre avant l'ouverture de la video pour trackbar :
    namedWindow( "couleur", CV_WINDOW_AUTOSIZE );
    bool vidCharge;
    int device = 0;

    // parametres
    if(argc == 2) {
        // id camera
        if(is_number(argv[1]))
           device = atoi(argv[1]);
        // video
        else {
            // On ouvre la video et on ajoute une trackbar pour se déplacer dans celle-ci
            cap = new VideoCapture(argv[1]);
            createTrackbar("frame", "couleur", NULL, (int) cap->get(CV_CAP_PROP_FRAME_COUNT), &cb_tbFrame);
            vidCharge = true;
        }
    }

    // ouverture camera
    if(!cap) {
        cap = new VideoCapture(device);
        vidCharge = false;
    }

    if(!cap->isOpened())
        die("erreur lors de l'ouverture du flux video.\n" );

    //on recupere la taille
    Mat frame;
    (*cap) >> frame;
    tailleVid = frame.size();

    //fenetre et trackbars
    namedWindow( "filtre1", CV_WINDOW_AUTOSIZE );
    createTrackbar("hmin[0]", "filtre1", &hmin[0], 180, NULL);
    createTrackbar("smin[0]", "filtre1", &smin[0], 255, NULL);
    createTrackbar("vmin[0]", "filtre1", &vmin[0], 255, NULL);
    createTrackbar("hmax[0]", "filtre1", &hmax[0], 180, NULL);
    createTrackbar("smax[0]", "filtre1", &smax[0], 255, NULL);
    createTrackbar("vmax[0]", "filtre1", &vmax[0], 255, NULL);

    // namedWindow( "filtre2", CV_WINDOW_AUTOSIZE );
    // createTrackbar("hmin[1]", "filtre2", &hmin[1], 180, NULL);
    // createTrackbar("smin[1]", "filtre2", &smin[1], 255, NULL);
    // createTrackbar("vmin[1]", "filtre2", &vmin[1], 255, NULL);
    // createTrackbar("hmax[1]", "filtre2", &hmax[1], 180, NULL);
    // createTrackbar("smax[1]", "filtre2", &smax[1], 255, NULL);
    // createTrackbar("vmax[1]", "filtre2", &vmax[1], 255, NULL);
    // namedWindow( "sortiefiltreor", CV_WINDOW_AUTOSIZE );

    namedWindow( "hough", CV_WINDOW_AUTOSIZE );
    createTrackbar("p1", "hough", &p1, 500, NULL);
    createTrackbar("p2", "hough", &p2, 500, NULL);

    createTrackbar("f1", "hough", &f1, 30, NULL);
    createTrackbar("f2", "hough", &f2, 30, NULL);

    createTrackbar("eros size", "hough", &erosion_size, 255, NULL);
    createTrackbar("type", "hough", &erosion_type, 2, NULL);

    //boucle
    std::vector<Vec3f> cercle;
    while ( 1 ) {
        if(!vidCharge)
            cap->grab();

        Mat hsv, gris, orange;
        cap->retrieve(frame, 0);
        Mat dessin = frame.clone();

        f1 = f1%2?f1:f1+1;
        cvtColor(frame, gris, CV_BGR2GRAY );
        GaussianBlur(gris, gris, Size(f1, f1), f2, f2 );

        cvtColor(frame, hsv, CV_BGR2HSV);
        GaussianBlur(hsv, hsv, Size(f1, f1), f2, f2 );
        inRange(hsv, Scalar(hmin[0],smin[0],vmin[0]), Scalar(hmax[0],smax[0],vmax[0]), orange);

        Mat element = getStructuringElement( erosion_type,
                                             Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                             Point( erosion_size, erosion_size ) );

        // dilate(orange, orange, element );
        // erode(orange, orange, element );


        if(p1 <= 0)
            p1 = 1;
        if(p2 <= 0)
            p2 = 1;


        //HoughCircles(gris, cercle, CV_HOUGH_GRADIENT, 2, gris.rows/4, p1, p2, 0, 0);
        detect(orange, cercle);

        if(capture) {
            circle(dessin, Point(40,40), 20, Scalar(0,0,255), -1);
            (*outVid) << frame;    //enregistrement video
        }

        for(uint i = 0; i < cercle.size(); i++) {
            Point c(cvRound(cercle[i][0]), cvRound(cercle[i][1]));
            int r = cvRound(cercle[i][2]);

            circle(dessin, c, 3, Scalar(0,255,0), -1, 8, 0);
            circle(dessin, c, r, Scalar(255,0,0), 3, 8, 0);

            moyenne.add((point){c.x, c.y, r});
            point moy = moyenne.calc();
            Point c2 (moy.x, moy.y);

            circle(dessin, c2, 3, Scalar(51,204,255), -1, 8, 0);
            circle(dessin, c2, moy.r, Scalar(0,0,255), 3, 8, 0);
            //printf("%d) %d %d %d\n", i, c.x, c.y, r);
        }

        if(capture && cercle.size() > 0) {
            const point p = {cercle[0][0], cercle[0][1]};
            pointcapture.push_back(p);
        }
        cercle.clear();

        // imshow("filtre1", out2 );
        // imshow("filtre2", out3 );
        // imshow("sortiefiltreor", out4 );
        imshow("couleur", dessin);
        imshow("filtre1", orange);

        int k = waitKey(10) & 255;
        if(!vidCharge && k == ' ')    //(pas de capture si la video est charge)
            cb_button();
        else if(k == 27)
            break;
    }

    delete outVid;
    delete cap;
    return 0;
}

// 142 30

// filtre hsv
// 0 129 102
// 14 235 239

// hough
// 98 78
// 3 3

// -----

// ->GRAY->FLOU
// hough
// 127 78
// 2 2


void detect(const Mat& m, std::vector<Vec3f>& v) {

    uchar old = 0;
    int lenBmax = 0;
    int xmax = -1;
    int ymax = -1;

    for(int i = 0; i < m.rows; i++) {
        const uchar* p = m.ptr(i);
        int x = -1;
        int lenB = 0;
        int lenN = 0;
        for(int j = 0; j < m.cols; j++) {
            uchar c = p[j];

            if(c != old) {
                lenB = lenN = 0;
            }
            if(c) {
                lenB++;
                x = j-lenB;
                if(lenB > lenBmax) {
                    lenBmax = lenB;
                    xmax = x;
                    ymax = i;
                }
            }

            else {
                lenN++;
            }
            old = c;
        }
    }

    if(xmax > 0) {
        int r = lenBmax/2;
        v.push_back(Vec3f(xmax+r, ymax, r));
    }
}

void write_capture() {
    char fn[256];
    sprintf(fn, "cap_%lu", time(0));
    FILE* h = fopen(fn, "w+");
    if(!h)
        die("impossible d'ecrire capture");
    for(uint i = 0; i < pointcapture.size(); i++) {
        fprintf(h, "%d %d\n", pointcapture[i].x, pointcapture[i].y);
    }
    fclose(h);
}

void cb_button() {
    if(capture) {
        write_capture();
        pointcapture.clear();
    } else {
        init_outVid();
    }

    capture = !capture;
}

void init_outVid() {
    delete outVid;

    outVid = new VideoWriter("outVid.avi", 0, 30, tailleVid, true);
    if( !outVid->isOpened() ) {
        die("echec ouverture video !\n");
    }
}

void cb_tbFrame(int pos, void*)
{
    double nbf = pos;
    cap->set(CV_CAP_PROP_POS_FRAMES, nbf);
    cap->grab();
}
