#ifndef CAMERA_H
#define CAMERA_H
#include <cmath>
#include <iostream>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "vector3d.h"

class camera{
	vector3d loc;
	float camPitch,camYaw;
	float movevel;
	float mousevel;
	bool mi,ismoved;
	void lockCamera();
	void moveCamera(float dir);
	void moveCameraUp(float dir);

public:
    camera();
    camera(const vector3d& loc, float yaw = 0, float pitch = 0, float mv = .2, float mov = .2);

    void Control();
    void UpdateCamera();
    vector3d getVector();
    vector3d getLocation();
    float getPitch();
    float getYaw();
    float getMovevel();
    float getMousevel();
    bool isMouseIn();
		
    void setLocation(const vector3d& vec);
    void lookAt(float pitch,float yaw);
    void mouseIn(bool b);
    void setSpeed(float mv,float mov);
		
    bool isMoved();
};

#endif
