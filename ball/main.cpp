#include "Interface.hpp"

void initGL();
void lectureCmd(int argc, char **argv);
void clearVectors();

int main(int argc, char **argv)
{
    //sol
    simu::ModelPlan *mSol = new simu::ModelPlan(btVector3(0, 0, 0),btVector3(0, 1, 0));
    simu::Objets::get().mObjets.push_back(mSol);
    simu::VuePlan *vSol = new simu::VuePlan(mSol);
    simu::Objets::get().vObjets.push_back(vSol);

    //
    lectureCmd(argc, argv);

    //fenetre
    //initGL();
    sfg::SFGUI sfgui;
    sfg::Desktop desktop;

    //GLCanvas3D
    float espace = 4.0;

    simu::Vue2D f2DFace("face", simu::Vue2D::face);
    desktop.Add(f2DFace.getWindow());
    f2DFace.getWindow()->SetPosition (sf::Vector2f(espace, espace));
    sf::FloatRect rFace = f2DFace.getWindow()->GetAllocation();

    simu::Vue2D f2DDessus("Dessus", simu::Vue2D::dessus);
    desktop.Add(f2DDessus.getWindow());
    f2DDessus.getWindow()->SetPosition (sf::Vector2f(rFace.left + rFace.width + espace, espace));

    simu::Vue2D f2DCote("Cote", simu::Vue2D::cote);
    f2DCote.getWindow()->SetPosition (sf::Vector2f(espace, rFace.top + rFace.height + espace));
    desktop.Add(f2DCote.getWindow());

    simu::Vue3D f3DFace("Vue3DFace", sf::Vector3f(0, 5, 100));    //titre, pos, angle
    f3DFace.getWindow()->SetPosition (sf::Vector2f(rFace.left + rFace.width + espace,
                                                   rFace.top + rFace.height + espace));
    desktop.Add(f3DFace.getWindow());

    simu::Vue3D f3DDessus("Vue3DDessus", sf::Vector3f(0, 100, 5));
    f3DDessus.getWindow()->SetPosition (sf::Vector2f(rFace.left + rFace.width + espace + 40,
                                                     rFace.top + rFace.height + espace + 40));
    desktop.Add(f3DDessus.getWindow());

    //Fenetre de boutons
    simu::PanBouton &pb = simu::PanBouton::get();
    pb.getWindow()->SetPosition(sf::Vector2f(f2DDessus.getWindow()->GetAllocation().left +
                                             f2DDessus.getWindow()->GetAllocation().width +
                                             espace, espace));
    desktop.Add(pb.getWindow());

    //Fenetre d'information
    simu::PanInfo &pi = simu::PanInfo::get();
    pi.getWindow()->SetPosition(sf::Vector2f(pb.getWindow()->GetAllocation().left,
                                             pb.getWindow()->GetAllocation().top +
                                             pb.getWindow()->GetAllocation().height + espace));
    desktop.Add(pi.getWindow());

    //Fenetre pour la capture
    simu::PanCapture pc;
    pc.getWindow()->SetPosition(sf::Vector2f(pi.getWindow()->GetAllocation().left + pi.getWindow()->GetAllocation().width + espace, pi.getWindow()->GetAllocation().top));

    pc.ajFen(&f3DFace);
    pc.ajFen(&f3DDessus);
    desktop.Add(pc.getWindow());

    //Fenetre de lecture
    desktop.Add(simu::PanLecture::get().getWindow());

    sf::Event e;
    sf::Clock c, ct;
    while(simu::Fenetre::get().isOpen())
    {
        double t = c.restart().asSeconds();

        //Evennements
        int deltaMolette = 0;
        while(simu::Fenetre::get().pollEvent(e))
        {
            desktop.HandleEvent(e);

            if(e.type == sf::Event::Closed ||
               (e.type == sf::Event::KeyPressed && e.key.code == sf::Keyboard::Escape ))
                simu::Fenetre::get().close();

            if (e.type == sf::Event::MouseWheelMoved)
                deltaMolette = e.mouseWheel.delta;

            if(e.type == sf::Event::KeyPressed && e.key.code == sf::Keyboard::A )
            {
                simu::Objets::get().clear(1);
                lectureCmd(argc, argv);
            }
        }

        //Model
        desktop.Update(t);
        f3DFace.update(deltaMolette);
        f3DDessus.update(deltaMolette);
        f2DDessus.update(deltaMolette);
        f2DFace.update(deltaMolette);
        f2DCote.update(deltaMolette);
        pc.update();
        simu::PanInfo::get().update();
        simu::PanLecture::get().update(t);

        for(unsigned int i = 0; i < simu::Objets::get().vObjets.size(); i++)
            simu::Objets::get().mObjets[i]->update(t);
        simu::Monde::get().getDynamWorld()->stepSimulation(t * 4, 7);

        //Affichage
        simu::Fenetre::get().clear();
        simu::Fenetre::get().resetGLStates();
        sfgui.Display(simu::Fenetre::get());
        simu::Fenetre::get().display();
    }

    //sauvegarde parametre
    if(argc == 1)
    {
        std::ofstream o((pc.getRepertoire() + "/cfg").c_str(), std::ios::out | std::ios::trunc);
        if(!o.is_open())
            std::cout << "erreur ouverture fichier : cfg";

        o << f3DFace.toString() << "\n";
        o << f3DDessus.toString() << "\n";
        o << pc.getRepertoire() << " " << pc.getPrefix() << " " << pc.getFormat();
        o << "\n";
        o.close();
    }

    clearVectors();
}

void initGL()
{
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glClearDepth(1.f);
}

void lectureCmd(int argc, char **argv)
{
    //balle avec un chemin (./sfguitest nomFichier)
    if(argc == 2)
    {
        std::cout << "ouverture fichier : " << argv[1] << std::endl;
        std::cout << "ajout de la balle avec le chemin donne" <<std::endl;
        simu::PanBouton::get().ajBalleC(argv[1]);
    }
    //balle avec position de départ et force (./sfguitest Px Py Pz Fx Fy Fz Resitution)
    else if(argc == 8)
    {
        float pX, pY, pZ, fX, fY, fZ, restit;
        sscanf(argv[1], "%f", &pX);
        sscanf(argv[2], "%f", &pY);
        sscanf(argv[3], "%f", &pZ);
        sscanf(argv[4], "%f", &fX);
        sscanf(argv[5], "%f", &fY);
        sscanf(argv[6], "%f", &fZ);
        sscanf(argv[7], "%f", &restit);

        std::cout << "Création de la balle avec la position : (" << pX << "," << pY << "," << pZ << ")"
                  <<", la force  : (" << fX << "," << fY << "," << fZ << ")"
                  <<" et la restitution  : "<< restit << "." <<std::endl;
        simu::PanBouton::get().ajBalle(5, sf::Vector3f(pX, pY, pZ), sf::Vector3f(fX, fY, fZ), 1.0, restit);
    }
}

void clearVectors()
{
    simu::Objets::get().clear();
}
