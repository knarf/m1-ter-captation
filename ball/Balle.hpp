#ifndef BALLE_H
#define BALLE_H

#include "Monde.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <climits>

namespace simu
{
    //MODEL
    class ModelObjet
    {
    public:
        virtual ~ModelObjet();

        const btRigidBody *getBody() const;
        const btCollisionShape *getShape() const;
        const btMotionState *getMotion() const;

        void setForce(const btVector3 &v);
        void update(float);
        void dontUpdate(bool b);
        void setPosition(const btVector3 &);
        btVector3 getPosition() const;

        //retourne null si aucun chemin n'a été défini
        std::vector<btVector3> *getChemin();
        void setPosChemin(int p);
        void updatePosChemin();

        //0 si ok, 1 si erreur
        static int lireChemin(const std::string &nomFichier, std::vector<btVector3> &chemin);

    private:
        ModelObjet(const ModelObjet &);
        ModelObjet operator=(const ModelObjet &);

    protected:
        ModelObjet();    //héritable seulement
        ModelObjet(const std::vector<btVector3> &chemin);

        btRigidBody *m_body;    //a init

        std::vector<btVector3> *m_chemin;    //dans le cas ou le chemin de la balle est connu
        uint m_posChemin;
        bool m_dontUpdate;

        float m_dernUpd;
    };

    class ModelBalle : public ModelObjet
    {
    public:
        //Si on veut laisser faire bullet
        ModelBalle(float r, const btVector3 &pos, float mass, float restitution = 1.0);
        //Si le chemin est connu
        ModelBalle(float r, const std::vector<btVector3> &chemin);
        //Si on veut diriger la balle avec des setPosition
        ModelBalle(float r);

        ~ModelBalle();

    private:
        ModelBalle(const ModelBalle &m);
        ModelBalle operator=(const ModelBalle &m);
    };

    class ModelPlan : public ModelObjet
    {
    public:
        //Argument non pris en compte dans l'affichage (TODO)
        ModelPlan(const btVector3 &origine = btVector3(0,0,0), const btVector3 &inclinaison = btVector3(0,1,0));
        ~ModelPlan();

    private:
        ModelPlan(const ModelPlan &m);
        ModelPlan operator=(const ModelPlan &m);

    private:
        static float RESTIT_SOL;
    };

    //VUE
    class VueObjet
    {
    public:
        VueObjet(const ModelObjet *m);
        virtual void draw() = 0;
        virtual ~VueObjet();

    protected:
        const ModelObjet *m_mObjet;
    };

    class VueBalle : public VueObjet
    {
    public:
        VueBalle(const ModelBalle *m);
        void draw();

    private:
        static GLUquadricObj *m_quad;
        float m_rayon;
    };

    class VuePlan : public VueObjet
    {
    public:
        VuePlan(const ModelPlan *m);
        void draw();
    };

    //S
    class Objets
    {
    public:
        ~Objets(){}

        static Objets &get();

        std::vector<ModelObjet*> mObjets;
        std::vector<VueObjet*> vObjets;

        //tout suprimer a partir de deb (pas de realoc)
        void clear(size_t deb=0);

    private:
        Objets(){}
        Objets(const Objets &);
        Objets &operator=(const Objets &);

        static Objets *instance;
    };
}

#endif    //BALLE_H
