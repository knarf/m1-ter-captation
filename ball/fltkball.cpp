#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/Fl_Radio_Light_Button.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Sys_Menu_Bar.H>
#include <FL/Fl_Window.H>
#include <FL/gl.h>
#include <stdlib.h>

class cube_box : public Fl_Gl_Window {
    void draw();
    int handle(int);
public:
    double lasttime;
    int wire;
    double size;
    double speed;
    cube_box(int x,int y,int w,int h,const char *l=0)
        : Fl_Gl_Window(x,y,w,h,l) {lasttime = 0.0;}
};

/* The cube definition */
float v0[3] = {0.0, 0.0, 0.0};
float v1[3] = {1.0, 0.0, 0.0};
float v2[3] = {1.0, 1.0, 0.0};
float v3[3] = {0.0, 1.0, 0.0};
float v4[3] = {0.0, 0.0, 1.0};
float v5[3] = {1.0, 0.0, 1.0};
float v6[3] = {1.0, 1.0, 1.0};
float v7[3] = {0.0, 1.0, 1.0};

#define v3f(x) glVertex3fv(x)

void drawcube(int wire) {
    /* Draw a colored cube */
    glBegin(wire ? GL_LINE_LOOP : GL_POLYGON);
    glColor3ub(0,0,255);
    v3f(v0); v3f(v1); v3f(v2); v3f(v3);
    glEnd();
    glBegin(wire ? GL_LINE_LOOP : GL_POLYGON);
    glColor3ub(0,255,255); v3f(v4); v3f(v5); v3f(v6); v3f(v7);
    glEnd();
    glBegin(wire ? GL_LINE_LOOP : GL_POLYGON);
    glColor3ub(255,0,255); v3f(v0); v3f(v1); v3f(v5); v3f(v4);
    glEnd();
    glBegin(wire ? GL_LINE_LOOP : GL_POLYGON);
    glColor3ub(255,255,0); v3f(v2); v3f(v3); v3f(v7); v3f(v6);
    glEnd();
    glBegin(wire ? GL_LINE_LOOP : GL_POLYGON);
    glColor3ub(0,255,0); v3f(v0); v3f(v4); v3f(v7); v3f(v3);
    glEnd();
    glBegin(wire ? GL_LINE_LOOP : GL_POLYGON);
    glColor3ub(255,0,0); v3f(v1); v3f(v2); v3f(v6); v3f(v5);
    glEnd();
}

void cube_box::draw() {
    lasttime = lasttime+speed;
    if (!valid()) {
        glLoadIdentity();
        glViewport(0,0,w(),h());
        glEnable(GL_DEPTH_TEST);
        glFrustum(-1,1,-1,1,2,10000);
        glTranslatef(0,0,-10);
        gl_font(FL_HELVETICA_BOLD, 16 );
    }
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    glRotatef(float(lasttime*1.6),0,0,1);
    glRotatef(float(lasttime*4.2),1,0,0);
    glRotatef(float(lasttime*2.3),0,1,0);
    glTranslatef(-1.0, 1.2f, -1.5);
    glScalef(float(size),float(size),float(size));
    drawcube(wire);
    glPopMatrix();
    gl_color(FL_GRAY);
    glDisable(GL_DEPTH_TEST);
    gl_draw(wire ? "Cube: wire" : "Cube: flat", -4.5f, -4.5f );
    glEnable(GL_DEPTH_TEST);
}

int cube_box::handle(int e) {
    switch (e) {
    case FL_ENTER: cursor(FL_CURSOR_CROSS); break;
    case FL_LEAVE: cursor(FL_CURSOR_DEFAULT); break;
    }
    return Fl_Gl_Window::handle(e);
}

void quit_cb (Fl_Widget* w, void*) {
    exit(0);
}

int main(int argc, char **argv) {
    Fl_Window *window = new Fl_Window(300, 300);
    window->begin();
    cube_box *cube = new cube_box(0, 20, 300, 280, 0);
    window->end();

    window->begin();
    static Fl_Menu_Item	items[] = {
        { "Menu", 0, 0, 0, FL_SUBMENU },
        { "Quitter", 0, quit_cb},
        { 0 },
    };
    Fl_Sys_Menu_Bar *menubar_;
    menubar_ = new Fl_Sys_Menu_Bar(0, 0, 60, 20);
    menubar_->box(FL_FLAT_BOX);
    menubar_->menu(items);
    window->end();

    cube->speed = 1.0;
    cube->size = 1.0;
    cube->wire = 0;
    window->label("cube");
    window->show(argc,argv);
    cube->show();

    for (;;) {
        if (window->visible())
            {if (!Fl::check()) break;}	// returns immediately
        else
            {if (!Fl::wait()) break;}	// waits until something happens
        cube->redraw();
    }
    return 0;
}
