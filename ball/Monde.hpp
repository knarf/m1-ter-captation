#ifndef MONDE_H
#define MONDE_H

#include <cstdlib>
#include <bullet/btBulletDynamicsCommon.h>

//Singleton, utiliser get pour récupérer l'instance
namespace simu
{
    class Monde
    {
    public:
        static Monde &get();

        btDynamicsWorld *getDynamWorld();

    private:
        Monde();
        ~Monde();
        Monde(const Monde &m);
        Monde operator=(const Monde &m);

    private:
        static float GRAVITE;
        static Monde *instance;

        btDynamicsWorld* m_world;
        btDispatcher* m_dispatcher;
        btCollisionConfiguration* m_collisionConfig;
        btBroadphaseInterface* m_broadphase;
        btConstraintSolver* m_solver;
    };
}

#endif    //MONDE_H
