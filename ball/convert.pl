#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Data::Dumper;

sub d {
    print Dumper @_;
    exit 0;
}

sub v3f {
    my $s = shift;
    $s =~ s/[^,0-9.-]//g;
    return map {$_ + 0.0000} split /,/, $s;
}

sub trans {
    my ($x1, $y1, $z1, $x2, $y2, $z2) = @_;
    return ($x1+$x2, $y1+$y2, $z1+$z2);
}

sub scale {
    my ($x1, $y1, $z1, $s) = @_;
    return ($x1*$s, $y1*$s, $z1*$s);
}

sub read_file {
    my $f = shift;
    my @data;
    my $min = 0;
    my $max = 0;
    my $i = 0;

    while(<$f>) {
        chomp;
        my @v = map {$_ + 0.00} split / +/;
        push @data, \@v;

        if($v[1] < $data[$min][1]) {
            $min = $i;
        }

        if($v[1] > $data[$max][1]) {
            $max = $i;
        }

        $i++;
    }

    return {'data' => \@data, 'min' => $min, 'max' => $max};
}

sub main {
    my %o;
    getopts('d:f:h', \%o);

    if($o{h} || !$o{d} || !$o{f}) {
        die "Usage: convert -d x,y,z -f x,y,z\n"
    }

    my @deb = v3f($o{d});
    my @fin = v3f($o{f});

    for(@ARGV) {
        open my $h, '<', $_ or die $!;
        my $f = read_file($h);
        my $miny = $f->{data}[$f->{min}][1];
        my $maxy = $f->{data}[$f->{max}][1];

        for(@{$f->{data}}) {
            my ($x, $y, $z) = @$_;
            $y -= $miny;
            $y = $y/($maxy-$miny)*($deb[1]-$fin[1])+$fin[1];
            printf "%.3f %.3f %.3f\n", $x, $y, $z;
        }
    }
}

&main();
