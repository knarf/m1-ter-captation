#ifndef INTERFACE_H
#define INTERFACE_H

#include <SFGUI/SFGUI.hpp>
#include <SFML/Graphics.hpp>
#include <fstream>
#include <sstream>
#include "Balle.hpp"

typedef unsigned char uchar;

namespace simu
{
    class Fenetre : public sf::RenderWindow
    {
    public:
        static Fenetre& get();
        ~Fenetre(){}

    private:
        static Fenetre *instance;

        Fenetre();
        Fenetre(const Fenetre&);
        Fenetre &operator=(const Fenetre&);

    public:
        const static int LARGEUR = 800;
        const static int HAUTEUR = 600;
    };

    class Grille
    {
    public:
        Grille(const sf::FloatRect &espace, float largeur);

        //!deplacement de la grille sans changer de position (offset)
        void deplacer(const sf::Vector2f &v);

        //position de la grille sur la fenetre
        void setPosition(const sf::Vector2f &v);
        void setEspace(const sf::FloatRect &fr);

        void augmenterLargeur(float i = 0.1);
        void diminuerLargeur(float i = 0.1);
        void setLargeur(float);
        float getLargeur() const;

        void setLevel(int level);

    private:
        static const int MIN_LARGEUR = 5;

        void update();
        void updateOffset();
        void suprLignes();

        std::vector<sfg::Primitive::Ptr> m_lignes;
        sf::FloatRect m_espace;
        sf::Vector2f m_posOff;    //decalage de la grille par rapport au bord haut gauche
        float m_largeur;          //largeur affiché
        float m_largeurOff;       //largeur en trop du a la largeur minimal
        float m_level;
    };

    class FenetreGL
    {
    public:
        FenetreGL(const std::string &titre);
        virtual ~FenetreGL(){}
        sfg::Window::Ptr getWindow() const;
        sfg::GLCanvas::Ptr getCanvas() const;
        virtual void update(int deltaMolette) = 0;
        sf::Image *screenshot() const;

    protected:
        virtual void draw() = 0;

    private:
        void focusOn();
        void focusOff();
//
    public:
        const static int LARGEUR_MIN = Fenetre::HAUTEUR/3;
        const static int HAUTEUR_MIN = Fenetre::HAUTEUR/3;
    protected:
        bool m_focus;
    private:
        sfg::Window::Ptr m_window;
        sfg::GLCanvas::Ptr m_canvas;
    };

    class Vue3D : public FenetreGL
    {
    public:
        const static float NEAR;

        Vue3D(const std::string &nom, const sf::Vector3f &posCam = sf::Vector3f(0, -5, 100));
        virtual ~Vue3D(){}
        void update(int deltaMolette);
        const sf::Vector3f &getPosCam() const;
        const sf::Vector3f &getAngleCam() const;

        std::string toString() const;

    protected:
        void draw();

    private:
        sf::Vector3f m_posCam;
        sf::Vector3f m_posCamZoom;
        float m_zoom;

        float m_matrixModelView[16];
        float m_matrixProjection[16];
    };

    class Vue2D : public FenetreGL
    {
    public:
        typedef enum {face,dessus,cote} typeVue;

        Vue2D(const std::string &titre, typeVue tv);
        virtual ~Vue2D(){}
        void update(int deltaMolette);

    protected:
        void draw();

    private:
        void levelChange();
        void positionChange();
        void dimentionChange();

    private:
        sf::Vector3f m_posCam;
        typeVue m_typeVue;
        Grille m_grille;

        int m_preLevel;
        sf::Vector2f m_prePos;
    };

    class EntryNumber
    {
    public:
        EntryNumber(const std::string &nom, float valDef=0);
        ~EntryNumber(){};

        sfg::Box::Ptr getBox() const;
        float getValeur() const;
        void setValeur(float f);

    private:
        void checkSaisie();

    private:
        sfg::Entry::Ptr m_entry;
        sfg::Box::Ptr m_box;
    };

    class PanBouton
    {
    public:
        static PanBouton &get();
        ~PanBouton(){}

        sfg::Window::Ptr getWindow();
        sfg::Button::Ptr getButtonAjBalle();
        sfg::Button::Ptr getButtonSuprBalle();

        void ajBalle(float rayon, const sf::Vector3f &p, const sf::Vector3f &f, float masse, float restitution);
        void ajBalle();
        void ajBalleR();
        void ajBalleC(const std::string &s);
        void ajBalleC();

    private:
        PanBouton();
        void suprTtBalles();

    private:
        static PanBouton *instance;

        sfg::Window::Ptr m_window;

        EntryNumber m_enFX;
        EntryNumber m_enFY;
        EntryNumber m_enFZ;

        EntryNumber m_enDX;
        EntryNumber m_enDY;
        EntryNumber m_enDZ;

        EntryNumber m_enRestit;
        EntryNumber m_enMasse;
        EntryNumber m_enRayon;

        sfg::Entry::Ptr m_nFichierChemin;
    };

    class PanInfo
    {
    public:
        static PanInfo &get();
        ~PanInfo();

        sfg::Window::Ptr getWindow();

        void ajoutCB(int id, const std::string &nomBalle);
        void videCB();

        void update();

    private:
        PanInfo();

    private:
        static PanInfo *instance;

        sfg::Window::Ptr m_window;
        sfg::ComboBox::Ptr m_cBalles;
        sfg::Label::Ptr m_lPosBalle;
    };


    class Capture
    {
    public:
        Capture(const std::vector<FenetreGL*> src, const std::string &rep,
                const std::string &prefix = "img_out", const std::string &format = "png");
        ~Capture();

        void update();

    private:
        const std::vector<FenetreGL*> m_src;

        //indexer par le numero des fenetres (m_src)
        std::vector< std::vector<sf::Image*> > m_images;

        std::string m_prefix;
        std::string m_format;
    };

    class PanCapture
    {
    public:
        PanCapture();
        ~PanCapture();

        sfg::Window::Ptr getWindow() const;
        void update();
        void ajFen(FenetreGL *f);
        std::string getRepertoire() const;
        std::string getPrefix() const;
        std::string getFormat() const;

    private:
        void capture();
        void captureEtAjout();

        //
        std::vector<FenetreGL*> m_fenGL;

        sfg::Window::Ptr m_window;

        sfg::Entry::Ptr m_eRep;
        sfg::Entry::Ptr m_ePre;
        sfg::Button::Ptr m_bCapture;
        sfg::Button::Ptr m_bCapEtAj;
        Capture *m_capture;
    };

    class PanLecture
    {
    public:
        static PanLecture &get();
        sfg::Window::Ptr getWindow() const;

        //t : le tmp depuis le dernier appel
        void update(float t);

        //balle avec un chemin
        void reset(ModelObjet *b);
        void setInvalide();
        bool isInvalide() const;

    private:
        PanLecture();
        PanLecture(const PanLecture&);
        PanLecture &operator=(const PanLecture&);
        static PanLecture *instance;

        void play();
        void scaleChange();

        sfg::Window::Ptr m_window;

        sfg::Scale::Ptr m_scale;
        sfg::Button::Ptr m_bPlay;
        bool m_play;
        EntryNumber m_eFps;

        ModelObjet *m_objet;

        float m_tmpDernUpd;
    };
}

//UTIL
//number to string
template <typename T> std::string nts(const T& t)
{
    std::ostringstream os;
    os<<t;
    return os.str();
}

#endif    //INTERFACE_H
