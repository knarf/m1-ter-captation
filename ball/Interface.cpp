#include "Interface.hpp"
#include <cstdlib>
#include <algorithm>

using namespace simu;

////////////////////////////////////////////////////////////////////
Fenetre *Fenetre::instance(0);
////////////////////////////////////////////////////////////////////
Fenetre& Fenetre::get()
{
    if(instance==0)
        instance = new Fenetre;

    return *instance;
}
////////////////////////////////////////////////////////////////////
Fenetre::Fenetre() :
    sf::RenderWindow(sf::VideoMode(LARGEUR,HAUTEUR),"Simu", sf::Style::Default , sf::ContextSettings(32))
{
    setVerticalSyncEnabled(true);
//    setFramerateLimit(30);
}
////////////////////////////////////////////////////////////////////
Grille::Grille(const sf::FloatRect &espace, float largeur) :
    m_espace(espace), m_posOff(0,0), m_largeur(largeur), m_largeurOff(0), m_level(0)
{
    if(m_largeur<MIN_LARGEUR)
        m_largeur = MIN_LARGEUR;

    update();
}
////////////////////////////////////////////////////////////////////
void Grille::deplacer(const sf::Vector2f &v)
{
    m_posOff.x += v.x;
    m_posOff.y += v.y;

    updateOffset();
    update();
    //moyen de faire un set position sur chaque ligne
}
////////////////////////////////////////////////////////////////////
void Grille::setPosition(const sf::Vector2f &v)
{
    m_espace.left = v.x;
    m_espace.top = v.y;

    update();
    //moyen de faire un set position sur chaque ligne
}
////////////////////////////////////////////////////////////////////
void Grille::setEspace(const sf::FloatRect &fr)
{
    m_espace.height = fr.height;
    m_espace.width = fr.width;
    update();
}
////////////////////////////////////////////////////////////////////
void Grille::augmenterLargeur(float i)
{
    if(m_largeurOff!=0)
    {
        m_largeurOff -= i;
        if(m_largeurOff<=0)
        {
            m_largeur += -m_largeurOff;
            m_largeurOff = 0;

            updateOffset();
            update();
        }
    }
    else
    {
        m_largeur += i;

        updateOffset();
        update();
    }
}
////////////////////////////////////////////////////////////////////
void Grille::diminuerLargeur(float i)
{
    if(m_largeurOff!=0)
        m_largeurOff += i;
    else
    {
        m_largeur -= i;
        if(m_largeur <= MIN_LARGEUR)
        {
            m_largeurOff = MIN_LARGEUR-m_largeur;
            m_largeur = MIN_LARGEUR;
        }
        else
        {
            updateOffset();
            update();
        }
    }
}
////////////////////////////////////////////////////////////////////
void Grille::setLargeur(float l)
{
    m_largeur = l;
    if(m_largeur <= MIN_LARGEUR)
        m_largeur = MIN_LARGEUR;

    update();
}
////////////////////////////////////////////////////////////////////
float Grille::getLargeur() const
{
    return m_largeur;
}
////////////////////////////////////////////////////////////////////
void Grille::setLevel(int level)
{
    m_level = level;
    for(size_t i=0; i < m_lignes.size(); i++)
        m_lignes[i]->SetLevel(level);
}
////////////////////////////////////////////////////////////////////
void Grille::update()
{
    suprLignes();
    int nbLigneL = m_espace.width/m_largeur;
    int nbLigneH = m_espace.height/m_largeur;
    //sfg::Renderer &r = sfg::Renderer::Get();
    sf::Color c = sf::Color(0, 0, 255, 100);

    float l = m_espace.left + m_posOff.x;
    if(m_posOff.x<=0)
        l+=m_largeur;
    for(int i=0; i<nbLigneL; i++)
    {
        //m_lignes.push_back(r.CreateLine(sf::Vector2f(l, m_espace.top), sf::Vector2f(l, m_espace.top + m_espace.height), c));
        //m_lignes.back()->SetLevel(m_level);
        l+=m_largeur;
    }

    //pareil dans dans la verticale
    float h = m_espace.top + m_posOff.y;
    if(m_posOff.y<=0)
        h+=m_largeur;
    for(int j=0; j<nbLigneH; j++)
    {
        //m_lignes.push_back(r.CreateLine(sf::Vector2f(m_espace.left, h), sf::Vector2f(m_espace.left + m_espace.width, h), c));
        //m_lignes.back()->SetLevel(m_level);
        h+=m_largeur;
    }
}
////////////////////////////////////////////////////////////////////
void Grille::updateOffset()
{
    while(m_posOff.x>=m_largeur)
        m_posOff.x -= m_largeur;
    while(m_posOff.x<=-m_largeur)
        m_posOff.x += m_largeur;

    while(m_posOff.y>=m_largeur)
        m_posOff.y -= m_largeur;
    while(m_posOff.y<=-m_largeur)
        m_posOff.y += m_largeur;
}
////////////////////////////////////////////////////////////////////
void Grille::suprLignes()
{
    sfg::Renderer &r = sfg::Renderer::Get();
    for(size_t i = 0; i<m_lignes.size(); i++)
        r.RemovePrimitive(m_lignes[i]);

    m_lignes = std::vector<sfg::Primitive::Ptr>();
}
////////////////////////////////////////////////////////////////////
FenetreGL::FenetreGL(const std::string &titre) :
    m_focus(false),
    m_window(sfg::Window::Create()),
    m_canvas(sfg::GLCanvas::Create())
{
    m_canvas->SetDesiredRefreshRate (100);
    m_canvas->SetRequisition(sf::Vector2f(LARGEUR_MIN, HAUTEUR_MIN));

    //Signaux
    m_canvas->GetCustomDrawCallbackSignal().Connect(&FenetreGL::draw,this);
    m_window->GetSignal(sfg::Widget::OnMouseEnter).Connect(&FenetreGL::focusOn, this);
    m_window->GetSignal(sfg::Widget::OnMouseLeave).Connect(&FenetreGL::focusOff, this);

    m_window->SetTitle(titre);
    m_window->Add(m_canvas);
}
////////////////////////////////////////////////////////////////////
sf::Image *FenetreGL::screenshot() const
{
    const sf::FloatRect& rWin = m_window->GetAllocation();
    const sf::FloatRect& rCan = m_canvas->GetAllocation();
    sf::Image s = Fenetre::get().capture();

    sf::Image *res = new sf::Image;
    res->create(rCan.width, rCan.height);
    res->copy(s, 0, 0, sf::IntRect(rWin.left+rCan.left, rWin.top+rCan.top, rCan.width, rCan.height), false);

    return res;
}
////////////////////////////////////////////////////////////////////
sfg::Window::Ptr FenetreGL::getWindow() const
{
    return m_window;
}
////////////////////////////////////////////////////////////////////
sfg::GLCanvas::Ptr FenetreGL::getCanvas() const
{
    return m_canvas;
}
////////////////////////////////////////////////////////////////////
void FenetreGL::focusOn()
{
    m_focus = true;
}
////////////////////////////////////////////////////////////////////
void FenetreGL::focusOff()
{
    m_focus = false;
}
////////////////////////////////////////////////////////////////////
const float Vue3D::NEAR = 1.f;
////////////////////////////////////////////////////////////////////
Vue3D::Vue3D(const std::string &nom, const sf::Vector3f &posCam) :
    FenetreGL(nom),m_posCam(posCam), m_posCamZoom(posCam), m_zoom(1)
{}
////////////////////////////////////////////////////////////////////
void Vue3D::update(int deltaMolette)
{
    if(!m_focus)
        return;

    if(deltaMolette !=0)
    {
        if(deltaMolette == -1)
            m_zoom += 0.1;
        else if(deltaMolette == 1)
        {
            m_zoom -= 0.1;
            if(m_zoom <= 0.1)    m_zoom = 0.1;
        }

        m_posCamZoom.x = m_posCam.x * m_zoom;
        m_posCamZoom.y = m_posCam.y * m_zoom;
        m_posCamZoom.z = m_posCam.z * m_zoom;
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        m_posCam.x++;
        m_posCamZoom.x = m_posCam.x * m_zoom;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        m_posCam.x--;
        m_posCamZoom.x = m_posCam.x * m_zoom;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        m_posCam.y++;
        m_posCamZoom.y = m_posCam.y * m_zoom;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        m_posCam.y--;
        m_posCamZoom.y = m_posCam.y * m_zoom;
    }
}
////////////////////////////////////////////////////////////////////
std::string Vue3D::toString() const
{
    std::string res("");

    //COORD
    res +=  nts(m_posCamZoom.x) + " " + nts(m_posCamZoom.y) + " " + nts(m_posCamZoom.z) + "\n" ;    // + NEAR;

    //MODELVIEW
    for(uint i=0; i<4; i++)
    {
        for(uint j=0; j<4; j++)
            res += nts(m_matrixModelView[i*4+j]) + " ";
        res += "\n";
    }
    res += "\n";

    //PROJECTION
    for(uint i=0; i<4; i++)
    {
        for(uint j=0; j<4; j++)
            res += nts(m_matrixProjection[i*4+j]) + " ";
        res += "\n";
    }

    return res;
}
////////////////////////////////////////////////////////////////////
void Vue3D::draw()
{
    GLfloat light_position[] = { 0.0, 0.0, 1.5, 1.0 };
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(100.f, 1.f, NEAR, 500.f);

    glMatrixMode(GL_MODELVIEW);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glShadeModel (GL_SMOOTH);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);

    gluLookAt(m_posCamZoom.x, m_posCamZoom.y, m_posCamZoom.z, 0, 0, 0, 0 , 1, 0);

    //sauvegarde des matrix
    glGetFloatv(GL_MODELVIEW_MATRIX, m_matrixModelView);
    glGetFloatv(GL_PROJECTION_MATRIX, m_matrixProjection);

    //z inutile
    for(unsigned int i = 0; i < Objets::get().vObjets.size(); i++)
        Objets::get().vObjets[i]->draw();

    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
    glDisable(GL_DEPTH_TEST);
}
////////////////////////////////////////////////////////////////////
const sf::Vector3f &Vue3D::getPosCam() const
{
    return m_posCam;
}
////////////////////////////////////////////////////////////////////
Vue2D::Vue2D(const std::string &titre, Vue2D::typeVue tv) :
    FenetreGL(titre),m_posCam(0, 0, 100), m_typeVue(tv),
    m_grille(getCanvas()->GetAllocation(), 40),
    m_preLevel(0), m_prePos(sf::Vector2f(0,0))
{
    getWindow()->GetSignal(sfg::Widget::OnSizeAllocate).Connect(&Vue2D::dimentionChange, this);
}
////////////////////////////////////////////////////////////////////
void Vue2D::update(int deltaMolette)
{
    //Changement de position / level (position dans l'affichage)
    int level = getCanvas()->GetHierarchyLevel();
    if(level != m_preLevel)
    {
        m_preLevel = level;
        levelChange();
    }
    sf::Vector2f pos(getCanvas()->GetAbsolutePosition());
    if(pos != m_prePos)
    {
        m_prePos = pos;
        positionChange();
    }

    /* evennement  : */
    if(!m_focus)
        return;

    //Coordonnees x,y et z selon la vue de face
    if(deltaMolette==-1)
    {
        m_grille.diminuerLargeur(5);
        m_posCam.z*=1.1;
    }
    else if(deltaMolette==1)
    {
        m_grille.augmenterLargeur(5);
        m_posCam.z*=0.9;
    }

    sf::Vector2f d(0,0);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        d.x++;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        d.x--;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        d.y--;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        d.y++;

    m_posCam.x += d.x;
    m_posCam.y += d.y;
    m_grille.deplacer(sf::Vector2f(d.x,-d.y));
}
////////////////////////////////////////////////////////////////////
void Vue2D::draw()
{
    GLfloat light_position[] = { 0.0, 0.0, 1.5, 1.0 };

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    float d = m_posCam.z;
    glOrtho(-d, d, -d, d, 0.001, 1000000);

    glMatrixMode(GL_MODELVIEW);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glShadeModel (GL_SMOOTH);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);

    glTranslatef(m_posCam.x, m_posCam.y, -100);
	if(m_typeVue == Vue2D::cote)
        glRotatef(90, 0,1,0);
	if(m_typeVue == Vue2D::dessus)
        glRotatef(90, 1,0,0);

    for(unsigned int i = 0; i < Objets::get().vObjets.size(); i++)
        Objets::get().vObjets[i]->draw();

    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
    glDisable(GL_DEPTH_TEST);
}
////////////////////////////////////////////////////////////////////
void Vue2D::levelChange()
{
    m_grille.setLevel(m_preLevel);
}
////////////////////////////////////////////////////////////////////
void Vue2D::positionChange()
{
    m_grille.setPosition(m_prePos);
}
////////////////////////////////////////////////////////////////////
void Vue2D::dimentionChange()
{
    m_grille.setEspace(getCanvas()->GetAllocation());
}
////////////////////////////////////////////////////////////////////
EntryNumber::EntryNumber(const std::string &nom, float valDef) :
    m_entry(sfg::Entry::Create()), m_box(sfg::Box::Create(sfg::Box::HORIZONTAL, 5.0f))
{
    std::ostringstream os;
    os << valDef;
    m_entry->SetText(os.str());

    sfg::Label::Ptr label(sfg::Label::Create(nom));
    m_entry->SetRequisition(sf::Vector2f(100,0));
    m_entry->GetSignal(sfg::Entry::OnTextChanged).Connect(&EntryNumber::checkSaisie, this);

    m_box->Pack(label);
    m_box->Pack(m_entry);
}
////////////////////////////////////////////////////////////////////
sfg::Box::Ptr EntryNumber::getBox() const
{
    return m_box;
}
////////////////////////////////////////////////////////////////////
float EntryNumber::getValeur() const
{
    std::string s = m_entry->GetText();
    float res = 0;
    sscanf(s.c_str(), "%f", &res);
    return res;
}
////////////////////////////////////////////////////////////////////
void EntryNumber::setValeur(float f)
{
    char s[255];
    sprintf(s, "%f", f);

    m_entry->SetText(s);
}
////////////////////////////////////////////////////////////////////
void EntryNumber::checkSaisie()
{
    sf::String s = m_entry->GetText();

    bool chang = false;
    bool pointTrouve = false;
    for(size_t i = 0; i<s.getSize(); i++)
    {
        if(s[i]=='.' && !pointTrouve)
            pointTrouve=true;
        else if((s[i]<'0' || s[i]>'9') && (i!=0 || s[i]!='-'))
        {
            chang = true;
            s.erase(i, 1);
        }
    }

    if(chang)
        m_entry->SetText(s);     //! stack overflow
}
////////////////////////////////////////////////////////////////////
PanBouton *PanBouton::instance(0);
////////////////////////////////////////////////////////////////////
PanBouton &PanBouton::get()
{
    if(instance == 0)
    {
        instance = new PanBouton();
        if(instance == 0)
            exit(EXIT_FAILURE);
    }
    return *instance;
}
////////////////////////////////////////////////////////////////////
PanBouton::PanBouton() :
    m_window(sfg::Window::Create(sfg::Window::TITLEBAR | sfg::Window::BACKGROUND)),
    m_enFX("force x"),m_enFY("force y"),m_enFZ("force z"),
    m_enDX("debut x"),m_enDY("debut y", 60),m_enDZ("debut z"),
    m_enRestit("restitution force",1.0),m_enMasse("masse",1.0),m_enRayon("rayon",5.0),
    m_nFichierChemin(sfg::Entry::Create("nom fichier"))
{
    m_window->SetTitle("Controles");

    //nom fichier
    sfg::Box::Ptr boxNfich(sfg::Box::Create(sfg::Box::HORIZONTAL));
    boxNfich->Pack(sfg::Label::Create("fichier"));
    m_nFichierChemin->SetRequisition(sf::Vector2f(100,0));
    boxNfich->Pack(m_nFichierChemin);

    //Boutons et Signaux
    sfg::Button::Ptr bAjBalle(sfg::Button::Create("Ajouter Balle (cond dep)"));
    sfg::Button::Ptr bAjBalleR(sfg::Button::Create("Ajouter Balle (rand)"));
    sfg::Button::Ptr bAjBalleC(sfg::Button::Create("Ajouter Balle (Chemin)"));
    sfg::Button::Ptr bAjBalleDC(sfg::Button::Create("Ajouter Balle (cdep et chemin)"));
    bAjBalle->GetSignal(sfg::Widget::OnLeftClick).Connect(&PanBouton::ajBalle,this);
    bAjBalleR->GetSignal(sfg::Widget::OnLeftClick).Connect(&PanBouton::ajBalleR,this);
    bAjBalleC->GetSignal(sfg::Widget::OnLeftClick).Connect(&PanBouton::ajBalleC,this);
    bAjBalleDC->GetSignal(sfg::Widget::OnLeftClick).Connect(&PanBouton::ajBalle,this);
    bAjBalleDC->GetSignal(sfg::Widget::OnLeftClick).Connect(&PanBouton::ajBalleC,this);

    sfg::Button::Ptr bSuprTtBalles(sfg::Button::Create("Suprimer Balles"));
    bSuprTtBalles->GetSignal(sfg::Widget::OnLeftClick).Connect(&PanBouton::suprTtBalles,this);

    //Pannel
    sfg::Box::Ptr box(sfg::Box::Create(sfg::Box::VERTICAL, 5.0f));

    box->Pack(m_enFX.getBox());
    box->Pack(m_enFY.getBox());
    box->Pack(m_enFZ.getBox());

    box->Pack(m_enDX.getBox());
    box->Pack(m_enDY.getBox());
    box->Pack(m_enDZ.getBox());

    box->Pack(m_enRestit.getBox());
    box->Pack(m_enMasse.getBox());
    box->Pack(m_enRayon.getBox());

    box->Pack(bAjBalle);

    box->Pack(sfg::Separator::Create());
    box->Pack(bAjBalleR);

    box->Pack(sfg::Separator::Create());
    box->Pack(boxNfich);
    box->Pack(bAjBalleC);

    box->Pack(sfg::Separator::Create());
    box->Pack(bAjBalleDC);

    box->Pack(sfg::Separator::Create());
    box->Pack(bSuprTtBalles);

    m_window->Add(box);
}
////////////////////////////////////////////////////////////////////
sfg::Window::Ptr PanBouton::getWindow()
{
    return m_window;
}
////////////////////////////////////////////////////////////////////
void PanBouton::ajBalle(float rayon, const sf::Vector3f &p, const sf::Vector3f &f, float masse, float restitution)
{
    m_enFX.setValeur(f.x);
    m_enFY.setValeur(f.y);
    m_enFZ.setValeur(f.z);

    m_enDX.setValeur(p.x);
    m_enDY.setValeur(p.y);
    m_enDZ.setValeur(p.z);

    m_enRestit.setValeur(restitution);
    m_enMasse.setValeur(masse);
    m_enRayon.setValeur(rayon);

    //r, x, y, z, masse, restit
    ModelBalle *mBalle = new ModelBalle(rayon, btVector3(p.x, p.y, p.z), masse, restitution);

    mBalle->setForce(btVector3(f.x, f.y, f.z));

    Objets::get().mObjets.push_back(mBalle);
    Objets::get().vObjets.push_back(new VueBalle(mBalle));

    PanInfo::get().ajoutCB(Objets::get().vObjets.size()-1 ,"BALLE");
}
////////////////////////////////////////////////////////////////////
void PanBouton::ajBalle()
{
    ajBalle(m_enRayon.getValeur(), sf::Vector3f(m_enDX.getValeur(), m_enDY.getValeur(), m_enDZ.getValeur()),
            sf::Vector3f(m_enFX.getValeur(), m_enFY.getValeur(), m_enFZ.getValeur()),
            m_enMasse.getValeur(), m_enRestit.getValeur());
}
////////////////////////////////////////////////////////////////////
void PanBouton::ajBalleC(const std::string &s)
{
    m_nFichierChemin->SetText(s);

    std::vector<btVector3> chem;
    if(ModelObjet::lireChemin(s, chem) == 1)
        return;

    ModelBalle *mBalle = new ModelBalle(5, chem);
    Objets::get().mObjets.push_back(mBalle);
    Objets::get().vObjets.push_back(new VueBalle(mBalle));

    PanInfo::get().ajoutCB(Objets::get().vObjets.size()-1 ,"BALLE");
    PanLecture::get().reset(Objets::get().mObjets.back());
}
////////////////////////////////////////////////////////////////////
void PanBouton::ajBalleC()
{
    ajBalleC(m_nFichierChemin->GetText());
}

static double random (double min, double max)
{
    if(max < min)
        std::swap(max, min);

    double r = rand()/(double)RAND_MAX;
    return min + (max-min)*r;
}
////////////////////////////////////////////////////////////////////
void PanBouton::ajBalleR()
{
    const double
        rayon = random(2, 12),
        x = random(-5*rayon, 5*rayon),
        y = random(5*rayon, 10*rayon),
        z = random(-5*rayon, 5*rayon),
        masse = rayon*10,
        rest = random(0.2, 0.8),
        fx = random(-20, 20),
        fy = 0,
        fz = random(-20, 20);

    ajBalle(rayon, sf::Vector3f(x, y, z), sf::Vector3f(fx, fy, fz), masse, rest);
}
////////////////////////////////////////////////////////////////////
void PanBouton::suprTtBalles()
{
    Objets::get().clear(1);    //!\ suprime tous sauf le sol
    PanInfo::get().videCB();
    PanLecture::get().setInvalide();
}
////////////////////////////////////////////////////////////////////
PanInfo *PanInfo::instance(0);
////////////////////////////////////////////////////////////////////
PanInfo &PanInfo::get()
{
    if(instance == 0)
    {
        instance = new PanInfo();
        if(instance == 0)
            exit(EXIT_FAILURE);
    }
    return *instance;
}
////////////////////////////////////////////////////////////////////
PanInfo::PanInfo():
    m_window(sfg::Window::Create()),
    m_cBalles(sfg::ComboBox::Create()),
    m_lPosBalle(sfg::Label::Create("(X, Y, Z)"))
{
    m_window->SetTitle("Informations");

    sfg::Box::Ptr cbox = sfg::Box::Create();
    cbox->Pack(m_cBalles);

    sfg::Box::Ptr lbox = sfg::Box::Create();
    lbox->Pack(m_lPosBalle);

    sfg::Box::Ptr boxGene(sfg::Box::Create(sfg::Box::VERTICAL));

    boxGene->Pack(cbox);
    boxGene->Pack(lbox);

    m_window->Add(boxGene);
}
////////////////////////////////////////////////////////////////////
PanInfo::~PanInfo()
{
}
////////////////////////////////////////////////////////////////////
sfg::Window::Ptr PanInfo::getWindow()
{
    return m_window;
}
////////////////////////////////////////////////////////////////////
void PanInfo::update()
{
    /* On recupere le text sélectionné */
    std::string s = m_cBalles->GetSelectedText();
    if( s == "" )
    {
        m_lPosBalle->SetText("(X, Y, Z)");
        return;
    }

    /* on recupère l'index */
    const char *su = strstr(s.c_str(), "_");
    if(su==0)
        std::cout<<"!";
    int num;
    sscanf(su, "_%d", &num);

    /* On recupère la position */
    char spos[255];
    btVector3 pos = Objets::get().mObjets[num]->getPosition();
    sprintf(spos, "(%f, %f, %f)", pos.getX(), pos.getY(), pos.getZ());
    m_lPosBalle->SetText(spos);
}
////////////////////////////////////////////////////////////////////
void PanInfo::ajoutCB(int id, const std::string &nomBalle)
{
    std::stringstream ss;
    ss << id;

    m_cBalles->AppendItem(nomBalle + "_" + ss.str());
}
////////////////////////////////////////////////////////////////////
void PanInfo::videCB()
{
    while(m_cBalles->GetItemCount() != 0)
        m_cBalles->RemoveItem(m_cBalles->GetStartItemIndex());
}
////////////////////////////////////////////////////////////////////
Capture::Capture(const std::vector<FenetreGL*> src, const std::string &rep, const std::string &prefix, const std::string &format) :
    m_src(src), m_prefix(rep + "/" + prefix), m_format(format)
{
    m_images.resize(m_src.size());
}
////////////////////////////////////////////////////////////////////
Capture::~Capture()
{
    sf::Image *pimg;
    for(uint i = 0; i<m_images.size(); i++)
    {
        std::string nomFen = m_src[i]->getWindow()->GetTitle();

        /* sauvegarde des images */
        for(uint j = 0; j<m_images[i].size(); j++)
        {
            char nom[255];
            sprintf(nom,"%s_%s_%u.%s", m_prefix.c_str(),nomFen.c_str(), j, m_format.c_str());

            pimg = m_images[i][j];
            if (!pimg->saveToFile(nom))
                std::cout << "echec lors de l'enregistrement de "<< nom << std::endl;

            delete pimg;
        }

        /* creation d'une video */
        /*
          ffmpeg -i img_out_Vue3D_%d.png -vcodec huffyuv test.avi
          rm *.png
        */
    }
}
////////////////////////////////////////////////////////////////////
void Capture::update()
{
    for(size_t i=0; i<m_src.size(); i++)
        m_images[i].push_back(m_src[i]->screenshot());
}
////////////////////////////////////////////////////////////////////
PanCapture::PanCapture() :
    m_window(sfg::Window::Create()),
    m_eRep(sfg::Entry::Create("film01")), m_ePre(sfg::Entry::Create("img_out")),
    m_bCapture(sfg::Button::Create("Capture !")),
    m_bCapEtAj(sfg::Button::Create("Capture et ajout")),
    m_capture(0)
{
    m_ePre->SetRequisition(sf::Vector2f(100,0));
    m_eRep->SetRequisition(sf::Vector2f(100,0));
    m_bCapture->GetSignal(sfg::Widget::OnLeftClick).Connect(&PanCapture::capture, this);
    m_bCapEtAj->GetSignal(sfg::Widget::OnLeftClick).Connect(&PanCapture::captureEtAjout, this);
    m_window->SetTitle("Capture");

    sfg::Label::Ptr lpre(sfg::Label::Create("Prefix image"));
    sfg::Box::Ptr bpre(sfg::Box::Create());
    bpre->Pack(lpre);
    bpre->Pack(m_ePre);

    sfg::Label::Ptr lrep(sfg::Label::Create("repertoire"));
    sfg::Box::Ptr brep(sfg::Box::Create());
    brep->Pack(lrep);
    brep->Pack(m_eRep);

    sfg::Box::Ptr bBout(sfg::Box::Create());
    bBout->Pack(m_bCapture);
    bBout->Pack(m_bCapEtAj);

    sfg::Box::Ptr bGene(sfg::Box::Create(sfg::Box::VERTICAL));

    bGene->Pack(brep);
    bGene->Pack(bpre);
    bGene->Pack(bBout);

    m_window->Add(bGene);
}
////////////////////////////////////////////////////////////////////
PanCapture::~PanCapture()
{}
////////////////////////////////////////////////////////////////////
sfg::Window::Ptr PanCapture::getWindow() const
{
    return m_window;
}
////////////////////////////////////////////////////////////////////
void PanCapture::update()
{
    if(m_capture==0)
        return;
    else
        m_capture->update();
}
////////////////////////////////////////////////////////////////////
void PanCapture::ajFen(FenetreGL *f)
{
    m_fenGL.push_back(f);
}
////////////////////////////////////////////////////////////////////
std::string PanCapture::getRepertoire() const
{
    return m_eRep->GetText();
}
////////////////////////////////////////////////////////////////////
std::string PanCapture::getPrefix() const
{
    return m_ePre->GetText();
}
////////////////////////////////////////////////////////////////////
std::string PanCapture::getFormat() const
{
    return std::string("png");
}
////////////////////////////////////////////////////////////////////
void PanCapture::capture()
{
    if(m_capture==0)
    {
        m_capture = new Capture(m_fenGL, m_eRep->GetText(), m_ePre->GetText(), "png");
        m_bCapture->SetLabel("Stop !");
        m_bCapEtAj->SetState(sfg::Widget::INSENSITIVE);
    }
    else
    {
        delete m_capture;
        m_capture = 0;
        m_bCapture->SetLabel("Capture !");
        m_bCapEtAj->SetState(sfg::Widget::NORMAL);
    }
}
////////////////////////////////////////////////////////////////////
void PanCapture::captureEtAjout()
{
    PanBouton::get().ajBalle();
    capture();
}
////////////////////////////////////////////////////////////////////
PanLecture *PanLecture::instance(0);
////////////////////////////////////////////////////////////////////
PanLecture::PanLecture() :
    m_window(sfg::Window::Create()), m_scale(sfg::Scale::Create(0, 100, 1)),
    m_bPlay(sfg::Button::Create("PLAY")), m_play(false), m_eFps("fps", 3), m_objet(0),
    m_tmpDernUpd(0)
{
    m_window->SetTitle("Lecture");
    m_window->SetRequisition(sf::Vector2f(100, 150));    //sinon le scale ne se met pas à la bonne echelle
    setInvalide();

    m_bPlay->GetSignal(sfg::Widget::OnLeftClick).Connect(&PanLecture::play,this);
    m_scale->GetSignal(sfg::Widget::OnLeftClick).Connect(&PanLecture::scaleChange,this);

    sfg::Box::Ptr b(sfg::Box::Create(sfg::Box::VERTICAL));
    b->Pack(m_scale);
    b->Pack(m_bPlay);
    b->Pack(m_eFps.getBox());

    m_window->Add(b);
}
////////////////////////////////////////////////////////////////////
PanLecture &PanLecture::get()
{
    if(instance == 0)
        instance = new PanLecture;
    return *instance;
}
////////////////////////////////////////////////////////////////////
sfg::Window::Ptr PanLecture::getWindow() const
{
    return m_window;
}
////////////////////////////////////////////////////////////////////
void PanLecture::update(float t)
{
    m_tmpDernUpd += t;
    float tmp = 1/m_eFps.getValeur();
    if(m_tmpDernUpd >= tmp)
    {
        m_tmpDernUpd -= tmp;

        if(m_objet == 0)
            return;

        if(m_play)
            m_scale->SetValue(m_scale->GetValue()+1);

        m_objet->setPosChemin(m_scale->GetValue());
        m_objet->updatePosChemin();
    }
}
////////////////////////////////////////////////////////////////////
void PanLecture::setInvalide()
{
    m_objet = 0;
    m_scale->SetState(sfg::Widget::INSENSITIVE);
    m_bPlay->SetState(sfg::Widget::INSENSITIVE);
}
////////////////////////////////////////////////////////////////////
bool PanLecture::isInvalide() const
{
    return m_objet == 0;
}
////////////////////////////////////////////////////////////////////
void PanLecture::reset(ModelObjet *b)
{
    m_scale->SetState(sfg::Widget::NORMAL);
    m_bPlay->SetState(sfg::Widget::NORMAL);

    if(m_objet != 0)
        m_objet->dontUpdate(false);


    if(b->getChemin() == 0)
    {
        std::cout << "chemin non existant, impossible de lire." << std::endl;
        return;
    }

    m_objet = b;
    m_scale->SetRange(0, m_objet->getChemin()->size());
    m_objet->dontUpdate(true);

    m_scale->SetValue(0);
    if(!m_play)
        play();
}
////////////////////////////////////////////////////////////////////
void PanLecture::play()
{
    m_play = !m_play;
    if(m_play)
        m_bPlay->SetLabel("PAUSE");
    else
        m_bPlay->SetLabel("PLAY");
}
////////////////////////////////////////////////////////////////////
void PanLecture::scaleChange()
{
    m_play = false;
    m_bPlay->SetLabel("PLAY");
}
////////////////////////////////////////////////////////////////////
