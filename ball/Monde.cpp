#include "Monde.hpp"

using namespace simu;

////////////////////////////////////////////////////////////////////
float Monde::GRAVITE = 9.8;
Monde *Monde::instance = 0;
////////////////////////////////////////////////////////////////////
Monde::Monde()
{
    m_collisionConfig = new btDefaultCollisionConfiguration();
	m_dispatcher = new btCollisionDispatcher(m_collisionConfig);
	m_broadphase = new btDbvtBroadphase();
	m_solver = new btSequentialImpulseConstraintSolver();
	m_world = new btDiscreteDynamicsWorld(m_dispatcher,m_broadphase,m_solver,m_collisionConfig);
	m_world->setGravity(btVector3(0,-GRAVITE,0));
}
////////////////////////////////////////////////////////////////////
Monde::~Monde()
{
    delete m_collisionConfig;
	delete m_dispatcher;
	delete m_broadphase;
	delete m_solver;
    delete m_world;
}
////////////////////////////////////////////////////////////////////
Monde &Monde::get()
{
    if(instance==0)
    {
        instance = new Monde();
        if(instance==0)
            exit(EXIT_FAILURE);
    }

    return *instance;
}
////////////////////////////////////////////////////////////////////
btDynamicsWorld *Monde::getDynamWorld()
{
    return m_world;
}
////////////////////////////////////////////////////////////////////
