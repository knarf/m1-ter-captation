#include "Balle.hpp"

using namespace simu;
////////////////////////////////////////////////////////////////////
ModelObjet::ModelObjet() :
    m_body(0), m_chemin(0), m_posChemin(1), m_dernUpd(0)
{}
////////////////////////////////////////////////////////////////////
ModelObjet::ModelObjet(const std::vector<btVector3> &chemin) :
    m_body(0), m_chemin(new std::vector<btVector3>(chemin)), m_posChemin(1), m_dontUpdate(false), m_dernUpd(0)
{
    if(m_chemin->size() == 0)
    {
        std::cout << "chemin incorrect\n";
        exit(EXIT_FAILURE);
    }
}
////////////////////////////////////////////////////////////////////
ModelObjet::~ModelObjet()
{
    Monde::get().getDynamWorld()->removeCollisionObject(m_body);
    delete m_body->getMotionState();
    delete m_body->getCollisionShape();
    delete m_body;

    delete m_chemin;
}
////////////////////////////////////////////////////////////////////
int ModelObjet::lireChemin(const std::string &nomFichier, std::vector<btVector3> &chemin)
{
    std::ifstream f(nomFichier.c_str(), std::ifstream::in);
    if(!f.is_open())
    {
        std::cout << "fichier incorect : " << nomFichier << std::endl;
        return 1;
    }

    while(true)
    {
        float x = 0.f, y = 0.f, z = 0.f;
        if(!(f >> x >> y >> z).good())
            break;

        chemin.push_back(btVector3(x, y, z));
    }

    //On fait rebondir la balle sur la table
    //On centre la balle sur l'axe des x
    // float totX = 0, totZ = 0;
    // for(uint i=0; i<chemin.size(); i++)
    // {
    //     if(chemin[i] == btVector3(0, 0, 0))
    //         continue;
    //
    //     totX += chemin[i].getX();
    //     totZ += chemin[i].getZ();
    // }
    //
    // float moyX = totX / chemin.size();
    // float moyZ = totZ / chemin.size();
    // for(uint i=0; i<chemin.size(); i++)
    // {
    //     chemin[i].setX(moyX - chemin[i].getX());
    //     chemin[i].setZ(moyZ - chemin[i].getZ());
    // }

    return 0;
}
////////////////////////////////////////////////////////////////////
void ModelObjet::dontUpdate(bool b)
{
    m_dontUpdate = b;
}
////////////////////////////////////////////////////////////////////
const btRigidBody *ModelObjet::getBody() const
{
    return m_body;
}
////////////////////////////////////////////////////////////////////
const btCollisionShape *ModelObjet::getShape() const
{
    return m_body->getCollisionShape();
}
////////////////////////////////////////////////////////////////////
const btMotionState *ModelObjet::getMotion() const
{
    return m_body->getMotionState();
}
////////////////////////////////////////////////////////////////////
void ModelObjet::setForce(const btVector3 &v)
{
    m_body->setLinearVelocity(v);
}
////////////////////////////////////////////////////////////////////
void ModelObjet::update(float tmp)
{
    if(m_dontUpdate)
        return;

    m_dernUpd += tmp;
    if(m_dernUpd >= 0.1)
    {
        m_dernUpd-=0.1;

        if(m_chemin != 0)
        {
            if(m_posChemin >= m_chemin->size())
                return;

            updatePosChemin();
            ++m_posChemin;
        }
    }
}
////////////////////////////////////////////////////////////////////
void ModelObjet::setPosition(const btVector3 &v)
{
    btTransform trans;
    m_body->getMotionState()->getWorldTransform(trans);
    trans.setOrigin(v);
    m_body->getMotionState()->setWorldTransform(trans);
}
////////////////////////////////////////////////////////////////////
btVector3 ModelObjet::getPosition() const
{
    if(m_chemin != 0)
        return (*m_chemin)[m_posChemin];
    else
        return m_body->getCenterOfMassPosition();
}
////////////////////////////////////////////////////////////////////
std::vector<btVector3> *ModelObjet::getChemin()
{
    return m_chemin;
}
////////////////////////////////////////////////////////////////////
void ModelObjet::setPosChemin(int p)
{
    m_posChemin = p;
}
////////////////////////////////////////////////////////////////////
void ModelObjet::updatePosChemin()
{
    if(m_chemin != 0)
        setPosition((*m_chemin)[m_posChemin]);
}
////////////////////////////////////////////////////////////////////
ModelBalle::ModelBalle(float r, const btVector3 &pos, float mass, float restitution)
{
    btTransform t;
    t.setIdentity();
    t.setOrigin(pos);
    btSphereShape *m_sphere = new btSphereShape(r);
    btVector3 inertia(0,0,0);
    if(mass!=0.0)
        m_sphere->calculateLocalInertia(mass,inertia);

    btDefaultMotionState *m_motion = new btDefaultMotionState(t);
    btRigidBody::btRigidBodyConstructionInfo info(mass, m_motion, m_sphere, inertia);
    m_body=new btRigidBody(info);
    m_body->setRestitution(restitution);
    Monde::get().getDynamWorld()->addRigidBody(m_body);
}
////////////////////////////////////////////////////////////////////
ModelBalle::ModelBalle(float r, const std::vector<btVector3> &chemin) :
    ModelObjet(chemin)
{
    btTransform t;
    t.setIdentity();
    t.setOrigin((*m_chemin)[0]);
    btSphereShape *m_sphere = new btSphereShape(r);
    btVector3 inertia(0,0,0);
    btDefaultMotionState *m_motion = new btDefaultMotionState(t);
    btRigidBody::btRigidBodyConstructionInfo info(0.0, m_motion, m_sphere, inertia);
    m_body=new btRigidBody(info);

    //objet kinematic pour pouvoir appliquer les transformations
    m_body->setCollisionFlags(m_body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT | btCollisionObject::CF_NO_CONTACT_RESPONSE);

    Monde::get().getDynamWorld()->addRigidBody(m_body);
}
////////////////////////////////////////////////////////////////////
ModelBalle::ModelBalle(float r)
{
    btTransform t;
    t.setIdentity();
    t.setOrigin((*m_chemin)[0]);
    btSphereShape *m_sphere = new btSphereShape(r);
    btVector3 inertia(0,0,0);
    btDefaultMotionState *m_motion = new btDefaultMotionState(t);
    btRigidBody::btRigidBodyConstructionInfo info(0.0, m_motion, m_sphere, inertia);
    m_body=new btRigidBody(info);

    //objet kinematic pour pouvoir appliquer les transformations
    m_body->setCollisionFlags(m_body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT | btCollisionObject::CF_NO_CONTACT_RESPONSE);

    Monde::get().getDynamWorld()->addRigidBody(m_body);
}
////////////////////////////////////////////////////////////////////
ModelBalle::~ModelBalle()
{
}
////////////////////////////////////////////////////////////////////
float ModelPlan::RESTIT_SOL = 1;
ModelPlan::ModelPlan(const btVector3 &origine, const btVector3 &inclinaison)
{
    btTransform t;
    t.setIdentity();
    t.setOrigin(origine);
    btStaticPlaneShape* plane = new btStaticPlaneShape(inclinaison,0);
    btMotionState* motion = new btDefaultMotionState(t);
    btRigidBody::btRigidBodyConstructionInfo info(0.0,motion,plane);
    m_body = new btRigidBody(info);
    m_body->setRestitution(RESTIT_SOL);
    Monde::get().getDynamWorld()->addRigidBody(m_body);
}
////////////////////////////////////////////////////////////////////
ModelPlan::~ModelPlan()
{
}
////////////////////////////////////////////////////////////////////
VueObjet::VueObjet(const ModelObjet *m) : m_mObjet(m)
{
}
////////////////////////////////////////////////////////////////////
VueObjet::~VueObjet()
{}
////////////////////////////////////////////////////////////////////
GLUquadricObj *VueBalle::m_quad = gluNewQuadric();

VueBalle::VueBalle(const ModelBalle *m) :
    VueObjet(m),m_rayon(0)
{
    if(m_mObjet->getShape()->getShapeType() != SPHERE_SHAPE_PROXYTYPE)
        exit(EXIT_FAILURE);

    m_rayon = ((btSphereShape*)m_mObjet->getShape())->getRadius();
}
////////////////////////////////////////////////////////////////////
void VueBalle::draw()
{
    GLfloat no_mat[] = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat mat_ambient[] = { 0.7, 0., 0., 1.0 };
//    GLfloat mat_ambient_color[] = { 0.8, 0., 0., 1.0 };
    GLfloat mat_diffuse[] = { 0.8, 0.1, 0.1, 1.0 };
    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat low_shininess[] = { 100 };

    btTransform t;
    m_mObjet->getMotion()->getWorldTransform(t);    //get the transform
    float mat[16];
    t.getOpenGLMatrix(mat); //OpenGL matrix stores the rotation and orientation
    glPushMatrix();
    glMultMatrixf(mat); //multiplying the current matrix with it moves the object in place
    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, low_shininess);
    glMaterialfv(GL_FRONT, GL_EMISSION, no_mat);
    gluSphere(m_quad, m_rayon, 20, 20);
    glPopMatrix();
}
////////////////////////////////////////////////////////////////////
VuePlan::VuePlan(const ModelPlan *m) : VueObjet(m)
{
//    if(m_mObjet->getShape()->getShapeType() != STATIC_PLANE_PROXYTYPE)
//        erreur
}
////////////////////////////////////////////////////////////////////
void VuePlan::draw()
{
    GLfloat no_mat[] = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat mat_ambient_blanc[] = { 0.7, 0.7, 0.7, 1.0 };
    GLfloat mat_diffuse_blanc[] = { 0.7, 0.7, 0.7, 1.0 };
    GLfloat mat_ambient_noir[] = { 0.01, 0.01, 0.01, 1.0 };
    GLfloat mat_diffuse_noir[] = { 0.01, 0.01, 0.01, 1.0 };
    const int max = 1000;

    btTransform t;
    m_mObjet->getMotion()->getWorldTransform(t);
    float mat[16];
    t.getOpenGLMatrix(mat);
    glPushMatrix();

    glMultMatrixf(mat); //translation,rotation
    glTranslatef(-max/2, 0, -max/2);

    glBegin(GL_QUADS);
    glNormal3f(0, 1, 0);
    glMaterialfv(GL_FRONT, GL_SPECULAR, no_mat);
    glMaterialfv(GL_FRONT, GL_SHININESS, no_mat);
    glMaterialfv(GL_FRONT, GL_EMISSION, no_mat);
    const int mw = 50;
    const int w = max/mw;
    for(int x = 0; x < max; x += w) {
        for(int y = 0; y < max; y += w) {
            if(((x+y)/w)%2) {
                glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient_blanc);
                glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse_blanc);
            } else {
                glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient_noir);
                glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse_noir);
            }
            glVertex3f(x,   0, y);
            glVertex3f(x+w, 0, y);
            glVertex3f(x+w, 0, y+w);
            glVertex3f(x,   0, y+w);

            glVertex3f(x,   -y,   0);
            glVertex3f(x+w, -y,   0);
            glVertex3f(x+w, -(y+w), 0);
            glVertex3f(x,   -(y+w), 0);

            glVertex3f(max/2,  -y,     x);
            glVertex3f(max/2,  -(y+w), x);
            glVertex3f(max/2,  -(y+w), x+w);
            glVertex3f(max/2,  -y,     x+w);
        }
    }
    glEnd();

    glPopMatrix();
}
////////////////////////////////////////////////////////////////////
Objets *Objets::instance = 0;
////////////////////////////////////////////////////////////////////
Objets &Objets::get()
{
    if(instance==0)
    {
        instance = new Objets();
        if(instance==0)
            exit(EXIT_FAILURE);
    }
    return *instance;
}
////////////////////////////////////////////////////////////////////
void Objets::clear(size_t deb)
{
    for(size_t i=deb; i<vObjets.size(); i++)
        delete vObjets[i];

    for(size_t i=deb; i<mObjets.size(); i++)
        delete mObjets[i];

    vObjets.resize(deb);
    mObjets.resize(deb);
}
////////////////////////////////////////////////////////////////////
