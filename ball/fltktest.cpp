#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Counter.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/Fl_Window.H>
#include <FL/gl.h>

#include <list>
#include <ctime>
#include <cstdlib>

#include "Balle.hpp"

float forceX = 0;
float forceY = 0;
float forceZ = 0;
std::list<ModelBalle*> mballes;
std::list<VueBalle*> vballes;
void fin();
//fct callback : 
void ajouterBalle(Fl_Widget* o, void*);
void changerForceX(Fl_Widget* o, void*);
void changerForceY(Fl_Widget* o, void*);
void changerForceZ(Fl_Widget* o, void*);

class cube_box : public Fl_Gl_Window {
    void draw();
    int handle(int);
public:
    int wire;
    double size;
    double speed;
    cube_box(int x,int y,int w,int h,const char *l=0)
        : Fl_Gl_Window(x,y,w,h,l) {}
};

int main(int argc, char **argv)
{
    srand(time(0));

    Fl_Window window(800, 600);
    
    //cadre affichage opengl
    window.begin();
    cube_box cube(20, 20, 500, 500, 0);
    window.end();
    
    //bouton
    window.begin();
    Fl_Button bouton(550, 150, 130, 30, "ajouter balle" );
    window.end();
    bouton.callback(ajouterBalle);

    //widget de saisi
    window.begin();
    Fl_Box box(500, 200, 130, 30, "force :");
    Fl_Counter counterX(550, 250, 130, 30, "x");
    Fl_Counter counterY(550, 300, 130, 30, "y");
    Fl_Counter counterZ(550, 350, 130, 30, "z");
    window.end();
    counterX.callback(changerForceX);
    counterY.callback(changerForceY);
    counterZ.callback(changerForceZ);
    
    //titre fenetre
    window.label("test");
    
    window.show(argc, argv);
    cube.show();

    for (;;)
    {
        if (window.visible())
            {if (!Fl::check()) break;}	// returns immediately
        else
            {if (!Fl::wait()) break;}	// waits until something happens

        cube.redraw();
        //
        Monde::get().getDynamWorld()->stepSimulation(1/60.0);
    }

    fin();
}

//-----
void fin()
{
    for(std::list<VueBalle*>::iterator it = vballes.begin(); it != vballes.end(); it++)
        delete *it;
    for(std::list<ModelBalle*>::iterator it = mballes.begin(); it != mballes.end(); it++)
        delete *it;
}

void ajouterBalle( Fl_Widget* o, void*) 
{
    ModelBalle *mb = new ModelBalle(5, 0, 60, 0, 1.0);  //r,x,y,z,masse (0 <=> statique)
    VueBalle *vb = new VueBalle(mb);
    mballes.push_back(mb);
    vballes.push_back(vb);

    mb->setForce(btVector3(forceX, forceY, forceZ));
}

void changerForceX(Fl_Widget* o, void*)
{
    Fl_Counter *c = dynamic_cast<Fl_Counter*>(o);
    forceX = c->value();
}

void changerForceY(Fl_Widget* o, void*)
{
    Fl_Counter *c = dynamic_cast<Fl_Counter*>(o);
    forceY = c->value();
}

void changerForceZ(Fl_Widget* o, void*)
{
    Fl_Counter *c = dynamic_cast<Fl_Counter*>(o);
    forceZ = c->value();
}

void cube_box::draw() {
    if (!valid()) {
        glLoadIdentity();
        glViewport(0,0,w(),h());
        glEnable(GL_DEPTH_TEST);
        glFrustum(-1,1,-1,1,2,10000);
        gl_font(FL_HELVETICA_BOLD, 16 );
    }
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    
    glTranslatef(0, -10, -100);    //futur camera ?
    for(std::list<VueBalle*>::iterator it = vballes.begin(); it != vballes.end(); it++)
         (*it)->draw();

    glPopMatrix();
    gl_color(FL_GRAY);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH_TEST);
}

int cube_box::handle(int e) {
    switch (e) {
    case FL_ENTER: cursor(FL_CURSOR_CROSS); break;
    case FL_LEAVE: cursor(FL_CURSOR_DEFAULT); break;
    }
    return Fl_Gl_Window::handle(e);
}
