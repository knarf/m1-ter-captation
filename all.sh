rm -rf film01 && \
mkdir -p film01 && \
ball/simulation && \
ffmpeg -i 'film01/img_out_Vue3DDessus_%d.png' -vcodec huffyuv film01/top.avi && \
ffmpeg -i 'film01/img_out_Vue3DFace_%d.png' -vcodec huffyuv film01/front.avi && \
cv-test/capture -t film01/top.avi -f film01/front.avi -r > film01/data && \
ball/convert.pl -d $1 -f $2 film01/data > film01/data_fix && \
ball/simulation
