#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd ('abs_path', 'getcwd');

my $DEBUG = 1;
my %opt;
my $PROC;

my $o = `cat /proc/cpuinfo | grep processor | tail -n 1`;
$o =~ /(\d+)/;
$PROC = $1 + 1;

sub make {
    dsys('make', "-j$PROC", @_);
}

sub msg {
    print "\033[32m[*] ", @_, "\033[m\n";
}

sub sys {
    if($DEBUG) {
        print "> ", join(" ", @_), "\n";
    }
    system @_;
    return $? >> 8;
}

sub dsys {
    return sys(@_) == 0;
}

getopts('f', \%opt);

my $dir = abs_path($0);
$dir =~ s[/[^/]+$][];
my $loc = "$dir/local";
my $dep = "$dir/dep";
my $sfml_dir = (<$dep/Laurent*>)[0];
my $sfgui_dir = "$dep/SFGUI";
my $openal_dir  = (<$dep/openal-*>)[0];
my $snd_dir = (<$dep/libsnd*>)[0];
my $sfml_url = 'https://github.com/LaurentGomila/SFML/tarball/master';
my $bullet_dir = (<$dep/bullet-*>)[0];
my $jpg_dir = (<$dep/jpeg-*>)[0];

sys 'mkdir', '-p', $loc, $dep;

# if(!$jpg_dir) {
#     msg "dl libjpeg...";
#     dsys('wget', '-O', 'jpg', 'http://downloads.sourceforge.net/project/libjpeg/libjpeg/6b/jpegsrc.v6b.tar.gz?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Flibjpeg%2Ffiles%2Flibjpeg%2F6b%2Fjpegsrc.v6b.tar.gz%2Fdownload&ts=1358786412&use_mirror=heanet') or die "dl fail";
#     sys 'unzip', 'jpg';
#     $jpg_dir = (<$dep/jpeg-*>)[0];
#     chdir $dir;
# }

sub fix_inline {
    my $fn = shift;
    my $new = "$fn.bak";
    my $ok = 0;

    open my $in, '<', $fn or die "fail fix $fn";
    open my $out, '>', $new or die "fail fix $fn";

    while(<$in>) {
	if(m{^//#define SHARED_PTR_PROFILE}) {
	    $_ = "#define SHARED_PTR_PROFILE\n";
	    $ok = 1;
	}
	print $out $_;
    }
    rename $new, $fn;
    
    if(!$ok) {
	msg "fix déjà appliqué";
    }
}

if(!$bullet_dir) {
    msg "dl bullet...";
    chdir $dep;
    sys 'wget', '-O', 'bullet', 'http://bullet.googlecode.com/files/bullet-2.81-rev2613.tgz';
    sys 'tar', 'xf', 'bullet';
    $bullet_dir = (<$dep/bullet-*>)[0];
    chdir $dir;
}

if(!-e "$loc/lib/libBulletCollision.so") {
    chdir $bullet_dir;
    dsys('./configure', '--prefix='.$loc) or die "config fail";
    make() or die "make fail";
    make('install');
    chdir $dir;
}


if(!$snd_dir) {
    msg "dl libsdnfile...";
    chdir $dep;
    sys 'wget', '-O', 'snd', 'http://www.mega-nerd.com/libsndfile/files/libsndfile-1.0.25.tar.gz';
    sys 'tar', 'xf', 'snd';
    $snd_dir = (<$dep/libsnd*>)[0];
    chdir $dir;
}

if(!-e "$loc/lib/libsndfile.so") {
    chdir $snd_dir;
    dsys('./configure', '--prefix='.$loc) or die "configure fail";
    make() or die "make fail";
    make('install');
    chdir $dir;
}

if(!$openal_dir) {
    msg "dl openal...";
    chdir $dep;
    sys 'wget', '-O', 'openal', 'http://kcat.strangesoft.net/openal-releases/openal-soft-1.13.tar.bz2';
    msg "unpack";
    sys 'tar', 'xf', 'openal';
    $openal_dir = (<$dep/openal-*>)[0];
    chdir $dir;
}

if(!-e "$loc/lib/libopenal.so") {
    chdir $openal_dir;
    dsys('cmake', '-DCMAKE_INSTALL_PREFIX:PATH='.$loc, '.') or die "cmake fail";
    make() or die "make fail";
    make('install') or die "make fail";
    chdir $dir;
}

if(!$sfml_dir) {
    msg "dl snapshot SFML 2.0 ...";
    chdir $dep;
    unlink 'sf';
    sys 'wget', '-O', 'sf', $sfml_url;
    msg "unpack";
    sys 'tar', 'xf', 'sf';
    unlink 'sf';
    $sfml_dir = (<$dep/Laurent*>)[0];
    chdir $dir;
}

if(!-f "$loc/lib/libsfml-system.so") {
    msg 'generate makefile for SFML...';
    chdir $sfml_dir;
    dsys('cmake', '-DCMAKE_INSTALL_PREFIX:PATH='.$loc, '.') or die "cmake fail";
    msg 'compile SFML...';
    make() or die "make fail";
    make('install') or die "make install fail";
    chdir $dir;
}

if(!-d $sfgui_dir) {
    chdir $dep;
    msg 'dl SFGUI';
    sys 'git', 'clone', 'git://boxbox.org/SFGUI.git';
    chdir $dir;
}

if(!-e "$loc/lib/libsfgui.so") {
    chdir $sfgui_dir;
    sys 'git', 'checkout', 'df1600a57fe81bfb';
    msg 'generate makefile for SFGUI';
    dsys ('cmake',
	  '-DCMAKE_MODULE_PATH:PATH='.$sfml_dir.'/cmake/Modules',
	  '-DSFML_ROOT:PATH='.$sfml_dir,
	  '-DCMAKE_INSTALL_PREFIX:PATH='.$loc,
	  '.') or die "cmake fail";

    if($opt{f}) {
	fix_inline("$sfgui_dir/include/SFGUI/SharedPtr.hpp");
    }
    make() or die "make fail";
    make('install') or die "make install fail";
}

msg "OOOOOOKKKKK!! :)))";

print "lancer:\nexport LD_LIBRARY_PATH=$loc/lib\n";

